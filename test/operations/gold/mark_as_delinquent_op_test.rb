require "test_helper"
require "shopify_interactions"
require "minitest/mock"

module Gold
  class MarkAsDelinquentOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def setup
      stub_request_to_get_admin_shop
    end

    # Ensure that an account can be marked as delinquent properly.
    def test_mark_as_delinquent_from_delayed_charge
      set_up_shop_as_billing!
      transition_to! :check_charge
      transition_to! :charge_missing
      transition_to! :delayed_charge, charge_id: 2000
      time_machine! 15
      mark_as_delinquent_and_verify!
    end

    def test_mark_as_delinquent_from_delayed_charge_declined
      set_up_shop_as_billing!
      transition_to! :check_charge
      transition_to! :charge_missing
      transition_to! :delayed_charge, charge_id: 2000
      transition_to! :delayed_charge_declined
      time_machine! 30
      mark_as_delinquent_and_verify!
    end

    # Ensure that the operation can be retried if it fails for some reason.
    def test_retry_mark_as_delinquent
      set_up_shop_as_billing!
      transition_to! :check_charge
      transition_to! :charge_missing
      transition_to! :delayed_charge, charge_id: 2000
      time_machine! 35

      # Assume that marking has been attempted before and `billing` was left
      # in the marked state.
      transition_to! :marked_as_delinquent

      mark_as_delinquent_and_verify!
    end

    def test_min_days_before_delinquent
      set_up_shop_as_billing!
      transition_to! :check_charge
      transition_to! :charge_missing
      transition_to! :delayed_charge, charge_id: 2000
      time_machine! 5

      refute billing.can_transition_to?(:marked_as_delinquent)
    end

    private

    def mark_as_delinquent_and_verify!
      outcome = verify_email_delivery do
        with_shopify_session do
          MarkAsDelinquentOp.new(billing).call
        end
      end
      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
      assert_equal :delinquent, billing.current_state
    end

    # Verify that an email is requested to be sent as part of marking an
    # account as delinquent.
    def verify_email_delivery(&block)
      # Mock out the email delivery part and verify that it was called later
      mock_delivery = MiniTest::Mock.new
      mock_delivery.expect(:deliver_now, true)
      result = BillingMailer.stub(:delinquent, mock_delivery, &block)
      mock_delivery.verify
      result
    end
  end
end

require "test_helper"
require "shopify_interactions"
require "minitest/mock"

module Gold
  class SelectTierOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def setup
      stub_request_to_get_admin_shop
    end

    def test_select_tier
      transition_to_terms!
      tier = Tier.find(:basic)
      outcome = with_shopify_session { SelectTierOp.new(billing, tier).call }
      assert outcome.ok?
      assert_instance_of Outcomes::ChargeNeeded, outcome
      assert_equal billing.current_state, :select_tier
      assert_equal billing.last_selected_tier, tier
      assert_in_delta billing.trial_starts_at, Time.current, 10
      assert_nil billing.tier
    end

    def test_select_tier_as_affiliate
      self.shopify_plan = "affiliate"
      transition_to_terms!
      tier = Tier.find(:basic)
      outcome = with_shopify_session { SelectTierOp.new(billing, tier).call }
      assert outcome.ok?
      assert_instance_of Outcomes::TierApplied, outcome
      assert_equal billing.current_state, :affiliate
      assert_equal billing.last_selected_tier, tier
      assert_in_delta billing.trial_starts_at, Time.current, 10
      refute_nil billing.tier
    end

    def test_select_tier_as_staff
      self.shopify_plan = "staff"
      transition_to_terms!
      tier = Tier.find(:basic)
      outcome = with_shopify_session { SelectTierOp.new(billing, tier).call }
      assert outcome.ok?
      assert_instance_of Outcomes::TierApplied, outcome
      assert_equal billing.current_state, :staff
      assert_equal billing.last_selected_tier, tier
      assert_in_delta billing.trial_starts_at, Time.current, 10
      refute_nil billing.tier
    end

    def test_select_free_tier
      transition_to_terms!
      tier = Tier.find(:free)
      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(body: { recurring_application_charges: {} }.to_json)
      outcome = with_shopify_session { SelectTierOp.new(billing, tier).call }
      assert outcome.ok?
      assert_instance_of Outcomes::TierApplied, outcome
      assert_equal billing.current_state, :billing
      assert_equal billing.last_selected_tier, tier
      assert_in_delta billing.trial_starts_at, Time.current, 10
      refute_nil billing.tier
    end

    def test_select_missing_tier
      transition_to_terms!
      tier = nil
      outcome = with_shopify_session { SelectTierOp.new(billing, tier).call }
      refute outcome.ok?
      assert_instance_of Outcomes::CannotSelectTier, outcome
      assert_equal :accepted_terms, billing.current_state
      assert_raises(Gold::UnknownTier) { billing.last_selected_tier }
      assert_nil billing.tier
    end

    def test_select_locked_tier_fails
      transition_to_terms!
      tier = Tier.find(:locked_basic)
      outcome = with_shopify_session { SelectTierOp.new(billing, tier).call }
      refute outcome.ok?
      assert_instance_of Outcomes::CannotSelectTier, outcome
      assert_equal :accepted_terms, billing.current_state
      assert_raises(Gold::UnknownTier) { billing.last_selected_tier }
      assert_nil billing.tier
    end

    def test_select_tier_without_being_qualified_fails
      transition_to_terms!
      tier = Tier.find(:basic)
      outcome = billing.stub(:qualifies_for_tier?, false) do
        with_shopify_session { SelectTierOp.new(billing, tier).call }
      end
      refute outcome.ok?
      assert_instance_of Outcomes::CannotSelectTier, outcome
      assert_equal :accepted_terms, billing.current_state
      assert_raises { billing.last_selected_tier }
      assert_nil billing.tier
    end

    def test_select_tier_again
      transition_to_terms!
      tier = Tier.find(:basic)
      with_shopify_session { SelectTierOp.new(billing, tier).call }

      second_tier = Tier.find(:pro)
      outcome = with_shopify_session do
        SelectTierOp.new(billing, second_tier).call
      end
      assert outcome.ok?
      assert_instance_of Outcomes::ChargeNeeded, outcome
      assert_equal :select_tier, billing.current_state
      assert_equal second_tier, billing.last_selected_tier
      assert_in_delta billing.trial_starts_at, Time.current, 10
      assert_nil billing.tier
    end

    # Merchant decides to change their mind and pick a different tier after
    # viewing Shopify's charge prompt.
    def test_select_tier_after_viewing_charge
      transition_to_terms!
      tier = Tier.find(:basic)
      with_shopify_session { SelectTierOp.new(billing, tier).call }
      transition_to! :sudden_charge, charge_id: 2000

      second_tier = Tier.find(:pro)
      outcome = with_shopify_session do
        SelectTierOp.new(billing, second_tier).call
      end
      assert outcome.ok?
      assert_instance_of Outcomes::ChargeNeeded, outcome
      assert_equal :select_tier, billing.current_state
      assert_equal second_tier, billing.last_selected_tier
      assert_in_delta billing.trial_starts_at, Time.current, 10
      assert_nil billing.tier
    end

    def test_select_tier_after_declining_charge
      transition_to_terms!
      tier = Tier.find(:basic)
      with_shopify_session { SelectTierOp.new(billing, tier).call }
      transition_to! :sudden_charge, charge_id: 2000
      transition_to! :sudden_charge_declined

      second_tier = Tier.find(:pro)
      outcome = with_shopify_session do
        SelectTierOp.new(billing, second_tier).call
      end
      assert outcome.ok?
      assert_instance_of Outcomes::ChargeNeeded, outcome
      assert_equal :select_tier, billing.current_state
      assert_equal second_tier, billing.last_selected_tier
      assert_in_delta billing.trial_starts_at, Time.current, 10
      assert_nil billing.tier
    end

    def test_change_tier
      original_tier = Tier.find(:basic)
      set_up_shop_as_billing! original_tier.id

      tier = Tier.find(:pro)
      outcome = with_shopify_session { SelectTierOp.new(billing, tier).call }
      assert outcome.ok?
      assert_instance_of Outcomes::ChargeNeeded, outcome
      assert_equal :change_tier, billing.current_state
      assert_equal tier, billing.last_selected_tier
      assert_equal original_tier, billing.tier
    end

    def test_change_tier_as_staff
      original_tier = Tier.find(:basic)
      set_up_shop_as_staff! original_tier.id

      tier = Tier.find(:pro)
      outcome = with_shopify_session { SelectTierOp.new(billing, tier).call }
      assert outcome.ok?
      assert_instance_of Outcomes::TierApplied, outcome
      assert_equal :staff, billing.current_state
      assert_equal tier, billing.last_selected_tier
      assert_equal tier, billing.tier
    end

    def test_change_tier_as_affiliate
      original_tier = Tier.find(:basic)
      set_up_shop_as_affiliate! original_tier.id

      tier = Tier.find(:pro)
      outcome = with_shopify_session { SelectTierOp.new(billing, tier).call }
      assert outcome.ok?
      assert_instance_of Outcomes::TierApplied, outcome
      assert_equal :affiliate, billing.current_state
      assert_equal tier, billing.last_selected_tier
      assert_equal tier, billing.tier
    end

    def test_change_tier_to_free_tier
      original_tier = Tier.find(:basic)
      set_up_shop_as_billing! original_tier.id

      tier = Tier.find(:free)
      charges = [
        {
          id: 1000,
          name: "Basic Plan",
          status: "active",
          price: "5.66"
        }
      ]
      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(body: { recurring_application_charges: charges }.to_json)
      stub_shopify_request(:delete, "recurring_application_charges/1000.json")

      outcome = with_shopify_session { SelectTierOp.new(billing, tier).call }
      assert outcome.ok?
      assert_instance_of Outcomes::TierApplied, outcome
      assert_equal :billing, billing.current_state
      assert_equal tier, billing.last_selected_tier
      assert_equal tier, billing.tier
      assert_in_delta billing.trial_starts_at, Time.current, 10
    end

    def test_change_tier_same_tier
      tier = Tier.find(:basic)
      set_up_shop_as_billing! tier.id

      outcome = with_shopify_session { SelectTierOp.new(billing, tier).call }
      assert outcome.ok?
      assert_equal :change_tier, billing.current_state
      assert_equal tier, billing.last_selected_tier
      assert_equal tier, billing.tier
    end

    def test_change_tier_again
      tier = Tier.find(:basic)
      set_up_shop_as_billing! tier.id

      second_tier = Tier.find(:pro)
      with_shopify_session { SelectTierOp.new(billing, second_tier).call }

      # Pick a different tier before accepting the charge
      third_tier = Tier.find(:advanced)
      outcome = with_shopify_session do
        SelectTierOp.new(billing, third_tier).call
      end
      assert outcome.ok?
      assert_instance_of Outcomes::ChargeNeeded, outcome
      assert_equal :change_tier, billing.current_state
      assert_equal third_tier, billing.last_selected_tier
      assert_equal tier, billing.tier
    end

    def test_change_tier_after_viewing_charge
      tier = Tier.find(:basic)
      set_up_shop_as_billing! tier.id

      # Change to a different tier and generate a charge
      second_tier = Tier.find(:pro)
      with_shopify_session { SelectTierOp.new(billing, second_tier).call }
      transition_to! :optional_charge, charge_id: 4000

      # Pick a different tier after viewing the charge
      third_tier = Tier.find(:advanced)
      outcome = with_shopify_session do
        SelectTierOp.new(billing, third_tier).call
      end
      assert outcome.ok?
      assert_instance_of Outcomes::ChargeNeeded, outcome
      assert_equal :change_tier, billing.current_state
      assert_equal third_tier, billing.last_selected_tier
      assert_equal tier, billing.tier
    end

    def test_executes_callback_for_affiliate
      self.shopify_plan = "affiliate"
      transition_to_terms!
      tier = Tier.find(:basic)

      mock = MiniTest::Mock.new
      mock.expect(:call, nil, [billing, tier.id])
      Gold.configuration.on_apply_tier = mock

      with_shopify_session { SelectTierOp.new(billing, tier).call }
      assert_mock mock
    ensure
      Gold.configuration.on_apply_tier = nil
    end

    def test_executes_callback_for_staff
      self.shopify_plan = "staff"
      transition_to_terms!
      tier = Tier.find(:pro)

      mock = MiniTest::Mock.new
      mock.expect(:call, nil, [billing, tier.id])
      Gold.configuration.on_apply_tier = mock

      with_shopify_session { SelectTierOp.new(billing, tier).call }
      assert_mock mock
    ensure
      Gold.configuration.on_apply_tier = nil
    end

    private

    def transition_to_terms!
      transition_to! :install
      transition_to! :accepted_terms
    end
  end
end

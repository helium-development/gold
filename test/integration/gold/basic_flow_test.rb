require "test_helper"
require "shopify_interactions"

# rubocop:disable Metrics/LineLength
module Gold
  class BasicFlowTest < ActionDispatch::IntegrationTest
    include ActiveJob::TestHelper
    include ShopifyInteractions
    include Engine.routes.url_helpers

    def setup
      OmniAuth.config.test_mode = true
      OmniAuth.config.add_mock(
        :shopify,
        uid: shop.shopify_domain,
        credentials: { token: shop.shopify_token }
      )
    end

    def teardown
      OmniAuth.config.mock_auth[:shopify] = nil
    end

    def test_whole_lifecycle_with_free_tier
      assert_nil billing
      install
      accept_terms
      select_tier_without_charges :free
      uninstall
    end

    def test_whole_lifecycle_with_paid_tier
      stub_request_to_get_admin_shop
      assert_nil billing
      install
      accept_terms
      select_tier :pro
      uninstall
    end

    # When a merchant sees a charge and directly navigates away
    def test_outstanding_charge
      assert_nil billing
      install
      accept_terms

      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(body: { recurring_application_charges: [] }.to_json)

      charge = {
        id: "453213456".to_i,
        name: "Gold Pro",
        status: "pending",
        confirmation_url: "https://#{shop.shopify_domain}/admin/approve?charge_id=1000"
      }

      stub_shopify_request(:post, "recurring_application_charges.json")
        .to_return(body: { recurring_application_charge: charge }.to_json)

      with_shopify_session do
        put gold_engine.select_tier_path, params: { tier: "pro" }
      end

      stub_shopify_request(:get, "recurring_application_charges/#{charge[:id]}.json")
        .to_return(body: { recurring_application_charge: charge }.to_json)

      # As if the user navigated directly in their browser, or closed tab
      get "/"

      assert_redirected_to gold_engine.outstanding_charge_path
      follow_redirect!
      assert_match "Outstanding app charge", response.body
    end

    def test_as_shopify_staff
      self.shopify_plan = "staff"
      assert_nil billing
      install
      accept_terms
      select_tier_without_charges :basic
      uninstall
    end

    private

    # Log in to the dummy app via Shopify's mechanism. This will trigger the
    # setup of Gold, if necessary.
    def login
      # Expect the Shopify gem to register webhooks.
      stub_shopify_request(:get, "webhooks.json")
        .to_return(body: { webhooks: [] }.to_json)
      stub_shopify_request(:post, "webhooks.json")
        .to_return(body: { webhook: { id: 1 } }.to_json)

      # This isn't necessary to request this page, but it mimics a real user's
      # flow.
      get shopify_app.login_path

      post shopify_app.login_path, params: { shop: shop.shopify_domain }

      # This redirects to /auth/shopify to begin the OAuth 2 authentication
      # flow.
      follow_redirect!

      # The mock endpoint redirects back to /auth/shopify/callback, as would
      # happen for users after a successful login.
      follow_redirect!

      # Finally, it continues on to whatever destination that the merchant was
      # originally intending to go to.
      follow_redirect!

      # The billing object should be created by this point.
      refute_nil billing
    end

    # Install the app (essentially, log in for the first time).
    def install
      login
      assert_includes %i[install reinstalled], billing.current_state

      # Assume that the merchant will need to accept terms.
      assert_redirected_to gold_engine.terms_path
    end

    # Accept the dummy app's terms of service.
    def accept_terms
      put gold_engine.terms_path, params: { accept: "Accept" }
      assert_redirected_to gold_engine.select_tier_path
    end

    # Select a tier that will not required a charge (either the tier is free or
    # the merchant does not need to pay right now).
    def select_tier_without_charges(tier_id)
      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(body: { recurring_application_charges: [] }.to_json)

      with_shopify_session do
        put gold_engine.select_tier_path, params: { tier: tier_id }
      end
      assert_redirected_to "/"

      billing.reload
      assert_equal Tier.find(tier_id), billing.tier
    end

    # Select a particular paid tier.
    def select_tier(tier_id)
      # Expect the existing charges to be examined.
      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(body: { recurring_application_charges: [] }.to_json)

      # Expect a charge to be created.
      confirmation_url = "https://#{shop.shopify_domain}/admin/approve?charge_id=1000"
      charge = {
        id: 1000,
        status: "pending",
        confirmation_url: confirmation_url
      }
      stub_shopify_request(:post, "recurring_application_charges.json")
        .to_return(body: { recurring_application_charge: charge }.to_json)

      with_shopify_session do
        put gold_engine.select_tier_path, params: { tier: tier_id }
      end

      # The merchant will be directed to Shopify to approve the charge.
      assert_redirected_to confirmation_url

      # Return from Shopify with an accepted charge.
      charge[:status] = "accepted"
      stub_shopify_request(:get, "recurring_application_charges/#{charge[:id]}.json")
        .to_return(body: { recurring_application_charge: charge }.to_json)
      stub_shopify_request(:post, "recurring_application_charges/#{charge[:id]}/activate.json")
        .to_return(status: 200)
      get gold_engine.process_charge_path(charge_id: charge[:id])

      # Ensure that the tier was set properly.
      billing.reload
      assert_equal Tier.find(tier_id), billing.tier
    end

    # Uninstall the dummy app.
    def uninstall
      perform_enqueued_jobs do
        post_as_webhook(
          "/webhooks/app_uninstalled",
          "app/uninstalled",
          shop.shopify_domain,
          {}.to_json
        )
      end
      assert_equal :uninstalled, billing.current_state
    end

    # Makes a valid webhook POST request.
    def post_as_webhook(path, topic, domain, content)
      hmac = Base64.strict_encode64(
        OpenSSL::HMAC.digest("sha256", ShopifyApp.configuration.secret, content)
      )
      post path,
           params: content,
           headers: {
             "Content-Type" => "application/json",
             "X-Shopify-Topic" => topic,
             "X-Shopify-Hmac-Sha256" => hmac,
             "X-Shopify-Shop-Domain" => domain
           }
    end

    # Returns a `Billing` instance that was created for the shop under test.
    # This does not create an instance, but only finds an existing one.
    def billing
      billing = Billing.find_by(shop: shop)
    end
  end
end
# rubocop:enable Metrics/LineLength

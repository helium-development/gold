require "test_helper"
require "shopify_interactions"

module Gold
  class UnsuspendOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    # Ensure that a billing shop can be unsuspended properly.
    def test_unsuspension_to_charge_needed
      set_up_shop_as_billing!

      # Suspend it
      transition_to! :marked_as_suspended
      transition_to! :suspended

      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charges: []
        }.to_json)

      outcome = with_shopify_session { UnsuspendOp.new(billing).call }
      refute outcome.ok?
      assert_instance_of Outcomes::MissingCharge, outcome
      assert_equal :charge_missing, billing.current_state
    end

    def test_unsuspension_to_billing_when_free
      set_up_shop_as_billing!(:free)

      # Suspend it
      transition_to! :marked_as_suspended
      transition_to! :suspended

      outcome = with_shopify_session { UnsuspendOp.new(billing).call }
      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
      assert_equal :billing, billing.current_state
    end

    # Ensure that an affiliate shop can be unsuspended properly.
    def test_unsuspension_to_affiliate
      self.shopify_plan = "affiliate"
      with_shopify_session do
        transition_to! :install
        transition_to! :accepted_terms
        transition_to! :select_tier, tier_id: Tier.find(:basic).id
        transition_to! :affiliate
      end

      # Suspend it
      transition_to! :marked_as_suspended
      transition_to! :suspended

      outcome = with_shopify_session { UnsuspendOp.new(billing).call }
      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
      assert_equal :affiliate, billing.current_state
    end
  end
end

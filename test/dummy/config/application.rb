require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)
require "gold"

module Dummy
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    base_url = URI("http://localhost:3000")
    routes.default_url_options = {
      protocol: base_url.scheme,
      host: base_url.host,
      port: base_url.port
    }
    config.action_mailer.default_url_options = routes.default_url_options
  end
end

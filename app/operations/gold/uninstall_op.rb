module Gold
  # Invoked when a shop has uninstalled this app.
  class UninstallOp
    include Outcomes

    def initialize(billing)
      @billing = billing
    end

    def call
      return Success.new if @billing.current_state == :uninstalled

      @billing.transition_to_or_stay_in!(:marked_as_uninstalled)
      @billing.transition_to!(:uninstalled)
      Gold.configuration.on_uninstall&.call(@billing)

      Success.new
    end
  end
end

require "test_helper"
require "shopify_interactions"
require "gold/apply_discount_op"

module Gold
  class ApplyDiscountOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    RETURN_URL = "https://xyz.heliumdev.com/shut_up_and_take_my_money".freeze

    def test_discount_applied
      tier = Tier.find(:easy_money_math)
      set_up_shop_as_billing!(tier.id)

      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charges: [{
            id: "6543234566",
            name: tier.id,
            price: tier.monthly_price.to_s,
            status: "active"
          }]
        }.to_json)

      stub = stub_shopify_request(:post, "recurring_application_charges.json")
      stub.to_return(status: 200, body: {
        recurring_application_charge: {
          id: "453286544553",
          name: tier.name,
          confirmation_url: "https://xyz.myshopify.com/admin/charges"
        }
      }.to_json)

      with_shopify_session do
        op = ApplyDiscountOp.new(billing, 20, RETURN_URL, false).call
        assert op.ok?
        assert_instance_of Outcomes::PendingCharge, op
      end

      json_requests_for_stub(stub) do |body|
        assert_equal "80.0", body["recurring_application_charge"]["price"]
      end
    end

    def test_discount_percentage_invalid_fails
      op = ApplyDiscountOp.new(billing, -50, RETURN_URL).call
      refute op.ok?
      assert_instance_of Outcomes::CannotApplyDiscount, op
    end

    def test_same_discount
      billing.update(discount_percentage: 20)

      op = ApplyDiscountOp.new(billing, 20, RETURN_URL).call
      assert op.ok?
      assert_instance_of Outcomes::SameDiscount, op
    end

    def test_discount_with_free_tier
      tier = Tier.find(:free)
      set_up_shop_as_billing!(tier.id)

      op = ApplyDiscountOp.new(billing, 20, RETURN_URL).call

      assert op.ok?
      assert_instance_of Outcomes::Success, op
    end
  end
end

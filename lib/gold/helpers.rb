module Gold
  module Helpers
    def admin_url(path=nil)
      "https://#{current_shop.domain}/admin/#{path}"
    end
  end
end

module Gold
  # Creates a charge for a shop.
  class ChargeOp
    MAX_ATTEMPTS = 3

    include Retries
    include Outcomes

    def initialize(billing, return_url, test_charge = Gold.configuration.test_charge?)
      @billing = billing
      @return_url = return_url
      @test_charge = test_charge
    end

    # Find a previous charge to use or create a new one
    def call
      return ChargeNotNeeded.new(:tier_is_free) if tier.free?
      return ChargeNotNeeded.new(:in_billing_state) if @billing.current_state == :billing

      charges = ShopifyAPI::RecurringApplicationCharge.all || []
      if (charge = find_matching_charge(charges, @billing, "active"))
        Gold.logger.info("[#{@billing.id}] There is an existing charge (#{charge.id}) " \
                          "ready to use; skip charge creation")

        transition_to_charge_state!(charge_id: charge.id)
        return ActiveCharge.new(charge.id)
      elsif (charge = find_matching_charge(charges, @billing, "accepted"))
        Gold.logger.info("[#{@billing.id}] There is an accepted charge (#{charge.id}) " \
                          "that we haven't registered yet; redirecting to that now")

        # Log that this charge is the one we want to use
        transition_to_charge_state!(charge_id: charge.id)
        return AcceptedCharge.new(charge.decorated_return_url)
      elsif (charge = find_matching_charge(charges, @billing, "pending"))
        Gold.logger.info("[#{@billing.id}] Reusing an existing charge: #{charge.id}")

        # Mark the charge as ready for the merchant to review
        transition_to_charge_state!(charge_id: charge.id)
        return PendingCharge.new(charge.confirmation_url)
      else
        # Construct a new RecurringApplicationCharge for the tier
        charge = build_charge(@billing, tier)

        request_with_retries do
          charge.save!
        end

        # Mark the charge as ready for the merchant to review
        transition_to_charge_state!(charge_id: charge.id)

        return PendingCharge.new(charge.confirmation_url)
      end
    end

    protected

    def tier
      @billing.last_selected_tier
    end

    # Attempt to transition to :sudden_charge, then :delayed_charge, else fail
    def transition_to_charge_state!(metadata)
      if @billing.transition_to_or_stay_in(:sudden_charge, metadata)
      elsif @billing.transition_to_or_stay_in(:delayed_charge, metadata)
      elsif @billing.transition_to_or_stay_in(:optional_charge, metadata)
      else
        message = "Cannot transition from '#{@billing.current_state}' " \
                  "to 'delayed_charge', 'optional_charge', or 'sudden_charge'"
        raise Statesman::TransitionFailedError, message
      end
    end

    def find_matching_charge(charges, billing, status)
      charges.find do |charge|
        charge.status == status &&
          charge.price.to_d == billing.calculate_price(tier) &&
          billing.trial_starts_at && # Trial must have already started
          (status == "active" || charge.return_url == @return_url) &&
          (charge.test || false) == @test_charge
      end
    end

    def build_charge(billing, tier)
      name = "#{Gold.configuration.app_name} #{tier.name}"

      if billing.discount_percentage > 0
        name += " (#{billing.discount_percentage}% discount)"
      end

      ShopifyAPI::RecurringApplicationCharge.new(
        name: name,
        price: billing.calculate_price(tier),
        return_url: @return_url,
        test: @test_charge,
        trial_days: billing.calculate_trial_days(tier)
      )
    end
  end
end

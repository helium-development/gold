require "test_helper"
require "shopify_interactions"

module Gold
  class ChargeOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def setup
      @return_url = "https://cf.heliumdev.com/billing"
      # rubocop:disable Metrics/LineLength
      @confirmation_url = "https://#{shop.shopify_domain}/admin/charges/confirm_recurring_application_charge?id=654381177&amp;signature=BAhpBHkQASc%3D--374c02da2ea0371b23f40781b8a6d5f4a520e77b"
      # rubocop:enable Metrics/LineLength

      stub_request_to_get_admin_shop
    end

    def set_up_shop_as_select_tier!(tier_id = :basic)
      with_shopify_session do
        transition_to!(:install)
        transition_to!(:accepted_terms)
        transition_to!(:select_tier, tier_id: tier_id)
        billing.update(trial_starts_at: Time.current)
      end
    end

    def stub_charges(charges)
      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charges: charges
        }.to_json)
    end

    def stub_no_current_charges
      stub_charges([])
    end

    def stub_post_charge(charge)
      stub_shopify_request(:post, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charge: charge
        }.to_json)
    end

    def charge_body(tier, id = nil, status = nil)
      charge = {
        name: tier.name,
        price: tier.monthly_price,
        return_url: @return_url,
        test: false,
        trial_days: tier.trial_days
      }

      if id
        charge[:id] = id
        charge[:decorated_return_url] = @return_url + "?charge_id=#{id}"
      end

      charge[:status] = status if status

      charge
    end

    def last_charge_id(billing)
      transition = billing.state_machine
                          .last_transition_to(:delayed_charge,
                                              :optional_charge,
                                              :sudden_charge)
      transition.metadata["charge_id"]
    end

    def test_sudden_charge
      tier = Tier.find(:basic)
      charge_id = "45676452341"
      set_up_shop_as_select_tier!(tier.id)
      charge = charge_body(tier)
      stub_no_current_charges

      stub_post_charge(charge.merge(id: charge_id,
                                    status: "pending",
                                    confirmation_url: @confirmation_url))
        .to_return(status: 500).times(2).then
        .to_return(status: 200)

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::PendingCharge, outcome
      assert_equal charge_id, last_charge_id(billing)
      assert_equal :sudden_charge, billing.current_state
    end

    def test_select_tier_from_reinstalled
      tier = Tier.find(:basic)
      set_up_shop_as_billing!(tier.id)
      transition_to!(:marked_as_uninstalled)
      transition_to!(:uninstalled)
      transition_to!(:reinstalled)

      outcome = with_shopify_session do
        SelectTierOp.new(billing, Tier.find(:pro)).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::ChargeNeeded, outcome
      assert_equal :pro, billing.last_selected_tier.id
    end

    def test_sudden_charge_after_expired
      tier = Tier.find(:basic)
      set_up_shop_as_select_tier!(tier.id)
      with_shopify_session do
        transition_to!(:sudden_charge, charge_id: 1000)
      end
      transition_to!(:sudden_charge_expired)

      expired_charge = charge_body(tier, 1000).merge(status: "expired")

      stub_charges([expired_charge])
      charge_id = 2000
      charge = charge_body(tier).merge(id: charge_id,
                                       status: "pending",
                                       confirmation_url: @confirmation_url)
      stub_post_charge(charge)

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::PendingCharge, outcome
      assert_equal charge_id, last_charge_id(billing)
      assert_equal :sudden_charge, billing.current_state
    end

    def test_sudden_charge_after_declined
      tier = Tier.find(:basic)
      set_up_shop_as_select_tier!(tier.id)
      with_shopify_session do
        transition_to!(:sudden_charge, charge_id: 2000)
      end
      transition_to!(:sudden_charge_declined)

      declined_charge = charge_body(tier, 1000).merge(status: "declined")
      stub_charges([declined_charge])

      charge_id = 3000
      charge = charge_body(tier).merge(id: charge_id,
                                       status: "pending",
                                       confirmation_url: @confirmation_url)
      stub_post_charge(charge)

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::PendingCharge, outcome
      assert_equal charge_id, last_charge_id(billing)
      assert_equal :sudden_charge, billing.current_state
    end

    def test_optional_charge
      tier = Tier.find(:basic)
      set_up_shop_as_billing!(tier.id)

      new_tier = Tier.find(:pro)
      optional_charge_id = "9874564589"
      transition_to!(:change_tier, tier_id: new_tier.id)

      stub_charges([charge_body(tier, "234564334", "active")])

      stub_post_charge(charge_body(new_tier, optional_charge_id)
        .merge(
          status: "pending",
          confirmation_url: @confirmation_url
        ))

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::PendingCharge, outcome
      assert_equal optional_charge_id, last_charge_id(billing)
      assert_equal :optional_charge, billing.current_state
    end

    def test_delayed_charge_from_auto_upgrade_tier
      tier = Tier.find(:basic)
      set_up_shop_as_billing!(tier.id)

      new_tier = Tier.find(:pro)
      new_charge_id = "9874564589"
      transition_to!(:calculate_usage)
      transition_to!(:auto_upgrade_tier, tier_id: new_tier.id)

      stub_charges([charge_body(tier, "234564334", "active")])

      stub_post_charge(charge_body(new_tier, new_charge_id)
        .merge(
          status: "pending",
          confirmation_url: @confirmation_url
        ))

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::PendingCharge, outcome
      assert_equal new_charge_id, last_charge_id(billing)
      assert_equal :delayed_charge, billing.current_state
    end

    def test_delayed_charge_from_charge_missing
      tier = Tier.find(:basic)
      set_up_shop_as_billing!(tier.id)

      new_tier = Tier.find(:pro)
      new_charge_id = "9874564589"
      transition_to!(:check_charge)
      transition_to!(:charge_missing)

      stub_no_current_charges

      stub_post_charge(charge_body(new_tier, new_charge_id)
        .merge(
          status: "pending",
          confirmation_url: @confirmation_url
        ))

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::PendingCharge, outcome
      assert_equal new_charge_id, last_charge_id(billing)
      assert_equal :delayed_charge, billing.current_state
    end

    def test_delayed_charge_from_affiliate
      tier = Tier.find(:basic)
      set_up_shop_as_affiliate!(tier.id)
      billing.shopify_plan_override = "shopify_plus"
      transition_to! :affiliate_to_paid

      delayed_charge_id = "9874564589"
      stub_no_current_charges

      stub_post_charge(charge_body(tier, delayed_charge_id)
        .merge(
          status: "pending",
          confirmation_url: @confirmation_url
        ))

      with_shopify_session do
        op = ChargeOp.new(billing, @return_url).call
        assert op.ok?
        assert_instance_of Outcomes::PendingCharge, op
        assert_equal delayed_charge_id, last_charge_id(billing)
      end

      assert_equal :delayed_charge, billing.current_state
    end

    def test_delayed_charge_after_expired
      tier = Tier.find(:basic)
      set_up_shop_as_billing!(tier.id)

      new_tier = Tier.find(:pro)
      expired_charge_id = "9874564589"
      transition_to!(:calculate_usage)
      transition_to!(:auto_upgrade_tier, tier_id: new_tier.id)
      transition_to!(:delayed_charge, charge_id: expired_charge_id)
      transition_to!(:delayed_charge_expired)

      stub_charges([
        charge_body(tier, "234564334", "active"),
        charge_body(new_tier, expired_charge_id, "expired")
      ])

      new_charge_id = "5123598125"
      stub_post_charge(charge_body(new_tier, new_charge_id)
        .merge(
          status: "pending",
          confirmation_url: @confirmation_url
        ))

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::PendingCharge, outcome
      assert_equal new_charge_id, last_charge_id(billing)
      assert_equal :delayed_charge, billing.current_state
    end

    def test_delayed_charge_after_declined
      tier = Tier.find(:basic)
      set_up_shop_as_billing!(tier.id)

      new_tier = Tier.find(:pro)
      declined_charge_id = "1489732"
      transition_to!(:calculate_usage)
      transition_to!(:auto_upgrade_tier, tier_id: new_tier.id)
      transition_to!(:delayed_charge, charge_id: declined_charge_id)
      transition_to!(:delayed_charge_declined)

      stub_charges([
        charge_body(tier, "234564334", "active"),
        charge_body(new_tier, declined_charge_id, "declined")
      ])

      new_charge_id = "9874564589"
      stub_post_charge(charge_body(new_tier, new_charge_id)
        .merge(
          status: "pending",
          confirmation_url: @confirmation_url
        ))

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::PendingCharge, outcome
      assert_equal new_charge_id, last_charge_id(billing)
      assert_equal :delayed_charge, billing.current_state
    end

    def test_delayed_charge_after_delinquent
      tier = Tier.find(:basic)
      set_up_shop_as_billing!(tier.id)

      new_tier = Tier.find(:pro)
      transition_to!(:calculate_usage)
      transition_to!(:auto_upgrade_tier, tier_id: new_tier.id)
      transition_to!(:delayed_charge, charge_id: 1000)
      time_machine! 16
      transition_to!(:marked_as_delinquent)
      transition_to!(:delinquent)

      stub_charges([
        charge_body(tier, "234564334", "active"),
        charge_body(new_tier, "1000", "expired")
      ])

      new_charge_id = "9874564589"
      stub_post_charge(charge_body(new_tier, new_charge_id)
        .merge(
          status: "pending",
          confirmation_url: @confirmation_url
        ))

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::PendingCharge, outcome
      assert_equal new_charge_id, last_charge_id(billing)
      assert_equal :delayed_charge, billing.current_state
    end

    # If a charge has already been activated with the same details, we should
    # just reuse that one
    def test_returns_matching_activated
      tier = Tier.find(:advanced)
      charge_id = "3040428176"

      set_up_shop_as_select_tier!(tier.id)

      charge = charge_body(tier, charge_id).merge(
        name: "Slightly Different Name",
        status: "active",
        confirmation_url: @confirmation_url
      )
      stub_charges([charge])

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::ActiveCharge, outcome
      assert_equal charge_id, last_charge_id(billing)
      assert_equal :sudden_charge, billing.current_state
    end

    # Shopify will sometimes return `null` for the `test` property
    def test_returns_matching_activated_with_nil_test
      tier = Tier.find(:advanced)

      set_up_shop_as_select_tier!(tier.id)

      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charges: [
            {
              id: "2942349832",
              name: "My Charge",
              price: tier.monthly_price,
              return_url: "https://example.com/activate",
              status: "active",
              test: nil
            }
          ]
        }.to_json)

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert_instance_of Outcomes::ActiveCharge, outcome
    end

    def test_returns_matching_accepted
      tier = Tier.find(:pro)
      charge_id = "2186604783"

      set_up_shop_as_select_tier!(tier.id)

      charge = charge_body(tier, charge_id).merge(
        status: "accepted",
        confirmation_url: @confirmation_url
      )
      stub_charges([charge])

      outcome = with_shopify_session do
        ChargeOp.new(billing, @return_url).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::AcceptedCharge, outcome
      assert_equal charge_id, last_charge_id(billing)
      assert_equal :sudden_charge, billing.current_state
    end

    def test_build_charge
      tier = Tier.new(
        "id" => "blaze",
        "name" => "Trail Blazer",
        "pricing" => {
          "monthly" => "15.00",
          "trial_days" => 10
        }
      )

      # Create a temp class to test protected methods
      test_charge_op = Class.new(ChargeOp) do
        public :build_charge # rubocop:disable Style/AccessModifierDeclarations
      end

      with_shopify_session do
        billing.trial_starts_at = 2.days.ago
        op = test_charge_op.new(billing, true, @return_url)
        charge = op.build_charge(billing, tier)

        assert_equal "#{Gold.configuration.app_name} #{tier.name}", charge.name
        assert_equal tier.monthly_price.to_s, charge.price.to_s
        assert_equal 8, charge.trial_days
      end
    end

    def test_charge_not_needed_for_free
      set_up_shop_as_select_tier!(Tier.find(:free).id)

      outcome = ChargeOp.new(billing, @return_url).call
      assert_instance_of Outcomes::ChargeNotNeeded, outcome
    end
  end
end

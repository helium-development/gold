module Gold
  # Invoked when Helium unsuspends a shop.
  class UnsuspendOp
    include Outcomes

    def initialize(billing)
      @billing = billing
    end

    def call
      # Transition to appropriate location
      if @billing.transition_to(:affiliate)
        Success.new
      elsif @billing.transition_to(:check_charge)
        CheckChargeOp.new(@billing).call
      else
        @billing.transition_to!(:billing)
        Success.new
      end
    end
  end
end

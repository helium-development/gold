module Gold
  module Admin
    # Controller for referrals
    class ReferralsController < AdminController
      def create
        referral = Referral.new(referral_params)

        if referral.save
          flash[:success] = "Created new referrer"
        else
          message = referral.errors.full_messages.join(", ")
          flash[:danger] = "Failed to create, #{message}"
        end

        redirect_to "#{request.path}/#{referral.id}"
      end

      def update
        referral = Referral.find(params[:id])

        if referral.update(referral_params)
          flash[:success] = "Update referrer"
        else
          message = referral.errors.full_messages.join(", ")
          flash[:danger] = "Failed to update, #{message}"
        end

        redirect_back(fallback_location: "/admin")
      end

      def delete
        referral = Referral.find(params[:id])

        referral.destroy!

        flash[:success] = "Poof! Code #{referral.code} no longer exists"

        redirect_to gold_admin_engine.referrals_path
      end

      def add_billing
        referral = Referral.find_by(id: params[:referral_id])
        billing = Billing.find(params[:billing_id])

        billing.update(referral: referral)

        flash[:success] = "Updated referral"

        redirect_back(fallback_location: "/admin")
      end

      private

      def referral_params
        params.require(:referral).permit(:contact_name,
                                         :contact_email,
                                         :company,
                                         :code,
                                         :discount_percentage)
      end
    end
  end
end

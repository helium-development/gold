Gold::Engine.routes.draw do
  controller :billing do
    get  'billing/setup'           => :setup_billing, :as => :billing_setup
    get  'billing/activate'        => :activate_charge, as: :activate_charge
  end
end

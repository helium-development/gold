class AddSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :shop, index: true, foreign_key: true
      t.integer    :charge_id, limit: 8
      t.string     :plan
      t.integer    :price
      t.integer    :trial_days
      t.date       :activated_on
      t.datetime   :cancelled_at

      t.timestamps
    end
  end
end

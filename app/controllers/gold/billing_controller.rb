module Gold
  # Handles all merchant billing interactions.
  class BillingController < AuthenticatedController
    include Concerns::MerchantFacing
    include Outcomes

    layout "gold/billing"

    rescue_from Statesman::TransitionFailedError do |e|
      billing = Billing.find_by!(shop_id: session[:shop_id])
      Gold.logger.error("Shop '#{billing.shop.shopify_domain}' failed to " \
                        "transtion '#{e}'")
      render "gold/billing/transition_error", layout: "gold/billing",
                                              status: :internal_server_error
    end

    rescue_from Gold::Exceptions::BillingNotFound do
      redirect_to shopify_app.login_path
    end

    # Show the terms of service.
    def terms
      @already_accepted = !billing.can_transition_to?(:accepted_terms)
    end

    # Handle acceptance or declination of the terms of service.
    def process_terms
      accepted = params[:accept].present?
      outcome = AcceptOrDeclineTermsOp.new(billing, accepted).call
      case outcome
      when AcceptedTerms
        params.permit!
        Gold.configuration.on_terms&.call(billing, params.to_h)
        redirect_to select_tier_url
      else
        @error = true
        render :terms
      end
    end

    # Show the possible tiers to select from.
    def tier
      billing # Expose the @billing variable to the view

      apply_referral_cookie unless billing.tier

      params[:id] = cookies[:gold_tier_id] if cookies[:gold_tier_id].present?

      @tiers = Tier.visible
    end

    # Process a tier selection.
    def select_tier
      @tiers = Tier.visible
      @tier =  Tier.find(params[:tier])
      outcome = SelectTierOp.new(billing, @tier).call
      case outcome
      when SameTier, TierApplied
        cookies.delete(:gold_tier_id)
        redirect_to Gold.configuration.after_tier_redirect_path
      when CannotSelectTier
        flash.now[:error] = "Your shop is not eligible for this plan"
        render :tier
      when ChargeNeeded
        handle_charge_outcome ChargeOp.new(billing, process_charge_url).call
      else
        raise "Not sure how to handle #{outcome} on tier selection"
      end
    end

    # Show the merchant that they have an outstanding charge and let them
    # approve/decline it.
    def outstanding_charge
      outcome = ResolveOutstandingChargeOp.new(billing).call

      case outcome
      when PendingCharge
        Gold.logger.info("[#{billing.id}] Charge is pending")
        @confirmation_url = outcome.confirmation_url
        # Will render view with confirmation URL
      when AcceptedCharge
        Gold.logger.info("[#{billing.id}] Charge is accepted")
        redirect_to outcome.return_url
      when MissingCharge
        Gold.logger.info("[#{billing.id}] Charge is missing")
        redirect_to missing_charge_url
      when ExpiredCharge
        Gold.logger.info("[#{billing.id}] Charge is expired")
        redirect_to expired_charge_url
      when ActiveCharge
        unless billing.current_state == :billing
          AcceptOrDeclineChargeOp.new(billing, outcome.charge_id).call

          unless ApplyTierOp.new(billing).call.ok?
            raise "Charge was #{accepted_outcome}, but should have been active"
          end

          Gold.logger.info("[#{billing.id}] Charge is ready")
        end

        redirect_to Gold.configuration.after_tier_redirect_path
      else
        raise "Not sure how to handle #{outcome} on outstanding charge"
      end
    end

    # Process a charge confirmation when a merchant is redirected back from
    # Shopify.
    def process_charge
      outcome = AcceptOrDeclineChargeOp.new(billing, params[:charge_id]).call

      case outcome
      when ActiveCharge
        ApplyTierOp.new(billing).call
        outer_redirect(Gold.configuration.after_tier_redirect_path)
      when DeclinedCharge
        redirect_to declined_charge_url
      when ExpiredCharge
        redirect_to expired_charge_url
      when Uninstalled
        redirect_to uninstalled_url
      when CannotProcessCharge
        redirect_to missing_charge_url
      end
    end

    # Show the merchant that the charge has expired and direct them to try
    # again.
    def declined_charge
      ensure_billing_is :sudden_charge_declined, :delayed_charge_declined
    end

    # Show the merchant that the charge has expired and direct them to try
    # again.
    def expired_charge
      ensure_billing_is :sudden_charge_expired, :delayed_charge_expired
    end

    # Explain to the merchant that their charge is missing and direct them to
    # try again.
    def missing_charge
    end

    # Allow the merchant to retry the charge when necessary.
    def retry_charge
      handle_charge_outcome ChargeOp.new(billing, process_charge_url).call
    end

    # Show the merchant a suspension page if they have been suspended.
    def suspended
      ensure_billing_is :marked_as_suspended, :suspended
    end

    # Show the merchant a message about them uninstalling the app if they are
    # still logged in.
    def uninstalled
      ensure_billing_is :marked_as_uninstalled,
                        :uninstalled,
                        :frozen,
                        :cleanup,
                        :done
    end

    private

    # If embedded app, redirect directly through the Shopify admin. This can provide
    # a better experience than the page rendering and JS kicking off a redirect
    def outer_redirect(redirect_path)
      if ShopifyApp.configuration.embedded_app
        shopify_domain = billing.shop.shopify_domain
        api_key = ShopifyApp.configuration.api_key
        fullpath = "/admin/apps/#{api_key}#{redirect_path}"
        redirect_to URI::HTTPS.build(host: shopify_domain, path: fullpath).to_s
      else
        redirect_to redirect_path
      end
    end

    # Redirect to the appropriate location based on the return value from
    # ChargeOp.
    def handle_charge_outcome(outcome)
      Gold.logger.info("[#{billing.id}] Handling charge outcome")

      case outcome
      when ChargeNotNeeded
        Gold.logger.info("[#{billing.id}] Charge is not needed")
        redirect_to main_app.root_url
      when ActiveCharge
        Gold.logger.info("[#{billing.id}] Charge is active")
        redirect_to main_app.root_url
      when AcceptedCharge
        Gold.logger.info("[#{billing.id}] Charge is accepted")
        redirect_to outcome.return_url
      when PendingCharge
        Gold.logger.info("[#{billing.id}] Charge is pending")
        fullpage_redirect_to outcome.confirmation_url
      end
    end

    def apply_referral_cookie
      reference_id = :gold_referral_id
      referral_tracking = cookies[reference_id]

      # Remove the cookie for future use
      cookies.delete(reference_id)

      # Don't continue if billing already has a referral or there's no tracking code
      return if billing.referral || !referral_tracking

      # Find the referral by ID
      referral = Gold::Referral.find_by(id: referral_tracking)

      # Don't continue if that referral code wasn't found
      return unless referral

      # Apply the referral to the billing object
      billing.referral = referral
      billing.update(
        discount_percentage: referral.discount_percentage || 0
      )
    end

    # Ensure that the current merchant is in one of the provided states or
    # redirect to the app's root URL.
    def ensure_billing_is(*states)
      return if states.include?(billing.current_state)

      redirect_to main_app.root_url
      throw :halt
    end
  end
end

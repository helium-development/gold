module Gold
  # Clean up resources associated with a store after a merchant has uninstalled
  # and a sufficient amount of time has passed.
  class CleanupOp
    include Outcomes

    def initialize(billing)
      @billing = billing
    end

    def call
      @billing.transition_to_or_stay_in!(:cleanup)

      Gold.configuration.on_cleanup&.call(@billing)

      @billing.transition_to!(:done)
      Success.new
    end
  end
end

module Gold
  module Billing
    def self.trial_days
      raise "No trial days set" unless ENV['TRIAL_DAYS']
      ENV['TRIAL_DAYS'].to_i
    end

    def active_charge
      @active_charge ||= ShopifyAPI::RecurringApplicationCharge.current
    end

    def create_charge(return_path)
      if monthly_price_in_dollars > 0
        price = monthly_price_in_dollars
      else
        price = plan_price_in_dollars
      end

      ShopifyAPI::RecurringApplicationCharge.create({
        name: billing_name,
        trial_days: trial_days_left,
        price: price,
        return_url: "#{return_path}",
        test: !Rails.env.production?
      })
    end

    # How many days should be on the next chage
    def trial_days_left
      _subscription = subscription

      # Have they ever had a subscription before?
      if _subscription
        if _subscription.trial_ended?
          _subscription.pro_rated_days
        else
          _subscription.trial_days_left
        end
      else
        Billing.trial_days
      end
    end

    def monthly_price_in_dollars
      monthly_price / 100.00
    end

    def plan_price_in_dollars
      self.class.plans[plan] / 100.00
    end

    # Is the shop on a paid plan?
    def paid_plan?
      self.class.plans[plan] > 0
    end

    # If the shop should be billed or not
    def requires_subscription?
      !affiliate? && billing_required && paid_plan?
    end

    # Do they need to pay?
    def payment_required?
      requires_subscription? && !active_subscription
    end

    # Should they have access?
    def subscribed?
      if billing_required
        subscription.present?
      else
        installed?
      end
    end

    def pro_rated_days
      subscription ? subscription.pro_rated_days : 0
    end

    # Get the most recent subsciption
    def subscription
      subscriptions.last
    end

    def active_subscription
      subscriptions.where(cancelled_at: nil).first
    end

    def subscribe(charge)
      Subscription.create!({
        shop_id: id,
        plan: plan,
        charge_id: charge.id,
        price: (charge.price.to_f * 100).to_i,
        trial_days: charge.trial_days,
        activated_on: charge.activated_on
      })
    end

    def cancel_subscriptions!
      subscriptions.active.update_all(cancelled_at: Time.now)
    end
  end
end

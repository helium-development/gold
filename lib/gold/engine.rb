module Gold
  class Engine < Rails::Engine
    engine_name 'gold'
    isolate_namespace Gold
  end
end

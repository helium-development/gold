require "gold/application_controller"

module Gold
  # Inherit from Shopify's authenticated module
  class AuthenticatedController < ApplicationController
    include ShopifyApp::Authenticated
  end
end

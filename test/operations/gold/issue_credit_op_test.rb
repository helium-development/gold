require "test_helper"
require "shopify_interactions"

module Gold
  class IssueCreditOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    # Ensure that a credit can be issued successfully for a shop.
    def test_successful_credit
      set_up_shop_as_billing!

      amount = "10.24"
      description = "Thanks for being a loyal customer!"
      test = false

      # Define the expected request and the response that should be used when
      # that request is matched.
      credit_request = {
        application_credit: hash_including(
          description: description,
          amount: amount,
          test: test
        )
      }
      credit_response = {
        application_credit: {
          id: rand(2**32),
          amount: amount,
          description: description,
          test: test
        }
      }
      stub = stub_shopify_request(:post, "application_credits.json")
             .with(body: credit_request)
             .to_return(status: 201, body: credit_response.to_json)

      with_shopify_session do
        # Issue a credit and assert that it was successful.
        op = IssueCreditOp.new(billing, amount, description, test)
        assert_instance_of Outcomes::Success, op.call
      end

      assert_requested stub
      assert_equal :billing, billing.current_state.to_sym
    end

    # If a store is not marked as paying (such as a free store), a credit may
    # not be applied.
    def test_credit_free_store_fails
      set_up_shop_as_free!

      amount = "10.24"
      description = "Thanks for being a loyal customer!"
      test = false

      op = IssueCreditOp.new(billing, amount, description, test).call
      assert_instance_of Outcomes::CannotIssueCredit, op
    end

    # We cannot credit a negative amount.
    def test_negative_credit_fails
      set_up_shop_as_billing!

      amount = "-1.00"
      description = "Why don't you pay us?"
      test = false

      op = IssueCreditOp.new(billing, amount, description, test)
      outcome = op.call
      assert_instance_of Outcomes::CannotIssueCredit, outcome
      assert_equal :amount_not_positive, outcome.reason
    end

    # We cannot provide a credit without a reason.
    def test_credit_missing_reason_fails
      set_up_shop_as_billing!

      amount = "1.00"
      description = ""
      test = false

      op = IssueCreditOp.new(billing, amount, description, test)
      outcome = op.call
      assert_instance_of Outcomes::CannotIssueCredit, outcome
      assert_equal :missing_reason, outcome.reason
    end

    def test_max_attempts_fails
      set_up_shop_as_billing!

      amount = "100.00"
      description = "Benjamin bonus"
      test = false

      stub_shopify_request(:post, "application_credits.json")
        .to_return(status: 500).times(3)

      with_shopify_session do
        op = IssueCreditOp.new(billing, amount, description, test)
        assert_raises(ActiveResource::ServerError) { op.call }
      end
    end

    def test_exceeded_amount_fails
      set_up_shop_as_billing!

      amount = "300.00"
      description = "We're trying to be nice"

      stub_shopify_request(:post, "application_credits.json")
        .to_return(status: 422, body: {
          errors: {
            base: [
              "Amount exceeded 30 day shop credit issue limit (0.00)"
            ]
          }
        }.to_json)

      with_shopify_session do
        op = IssueCreditOp.new(billing, amount, description, false)
        outcome = op.call
        assert_instance_of Outcomes::CannotIssueCredit, outcome
        assert_equal :rejected_by_shopify, outcome.reason
        pp outcome.message
      end
    end
  end
end

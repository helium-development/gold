class AddForeignKeyToBilling < ActiveRecord::Migration[5.2]
  def change
    # TODO (nick/andrew) make this from a generator, which takes the
    # Gold.shop_class and uses the correct table as the primary key
    add_foreign_key :gold_billings, :shops, column: :shop_id, primary_key: :id
    #                               |^^^^^| change this
  end
end

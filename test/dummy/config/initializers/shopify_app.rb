ShopifyApp.configure do |config|
  config.application_name = "Gold-enabled App"
  config.api_key = "some-fake-api-key"
  config.secret = "some-fake-secret"
  config.scope = "read_orders, read_products"
  config.embedded_app = false
  config.after_authenticate_job = { job: Gold::AfterAuthenticateJob, inline: true }
  config.shop_session_repository = Shop

  webhook_base_url = URI(ENV["WEBHOOK_BASE_URL"])
  config.webhooks = [
    {
      topic: "app/uninstalled",
      address: (webhook_base_url + "webhooks/app_uninstalled").to_s
    }
  ]
end

var $tierForm = document.querySelector('.tiers form');

function tierStep($form) {
  var $tiers = $form.querySelectorAll('input[name="tier"]');

  function setSelectPlanButtonState() {
    var $selectedTier = $form.querySelector('input[name="tier"]:checked'),
        $submit = $form.querySelector('button[type="submit"]');
  
    if ($selectedTier) {
      $submit.removeAttribute('disabled');
      $submit.classList.remove('Polaris-Button--disabled');
    } else {
      $submit.setAttribute('disabled', 'disabled');
      $submit.classList.add('Polaris-Button--disabled');
    }
  }

  for (var i = 0; i < $tiers.length; i++) {
    $tiers[i].addEventListener('change', function(e) {
      setSelectPlanButtonState();
    });
  }
  
  setSelectPlanButtonState();
}

if ($tierForm) {
  tierStep($tierForm);
}

// When embedded
if (window.self !== window.top) {
  var $confirmation = document.getElementById('confirmation-button');

  if ($confirmation) {
    $confirmation.addEventListener('click', function(e) {
      e.preventDefault();
      app.dispatch(actions.Redirect.toRemote({
        url: $confirmation.getAttribute('href')
      }));
    });
  }
}
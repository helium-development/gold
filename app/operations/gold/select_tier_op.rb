module Gold
  module Outcomes
    CannotSelectTier = Class.new(Failure)
    SameTier = Class.new(Success)
  end

  # Allows a merchant to select their initial tier or change their tier.
  class SelectTierOp
    include Outcomes

    def initialize(billing, tier, enforce_locking = true)
      @billing = billing
      @tier = tier
      @enforce_locking = enforce_locking
    end

    def call
      @billing.transaction do
        return CannotSelectTier.new(:nil) unless @tier
        return CannotSelectTier.new(:locked) if @enforce_locking && @tier.locked?

        if (@billing.tier != @tier) && !@billing.qualifies_for_tier?(@tier)
          return CannotSelectTier.new(:disqualified)
        end

        # Mark the tier as selected
        if @billing.transition_to(:select_tier, tier_id: @tier.id)
        elsif @billing.transition_to(:change_tier, tier_id: @tier.id)
        else
          message = "Cannot transition from '#{@billing.current_state}' " \
                    "to 'select_tier' or 'change_tier'"
          raise Statesman::TransitionFailedError, message
        end

        # Mark the beginning of the trial if it hasn't been set already
        @billing.update(trial_starts_at: Time.current) unless @billing.trial_starts_at
      end

      # Process the tier selection, creating charges if necessary
      if @billing.shopify_plan.affiliate? ||
         @billing.shopify_plan.staff? ||
         @tier.free?

        # If the shop doesn't need a charge, apply the tier
        return ApplyTierOp.new(@billing).call
      else
        # A valid charge is necessary before applying the new tier
        return ChargeNeeded.new
      end
    end
  end
end

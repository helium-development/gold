module Gold
  # Invoked when Helium suspends a merchant for violating ToS.
  class SuspendOp
    include Outcomes

    def initialize(billing)
      @billing = billing
    end

    def call
      @billing.transition_to_or_stay_in!(:marked_as_suspended)

      # Communicate suspension to the merchant.
      BillingMailer.suspension(@billing).deliver_now

      @billing.transition_to!(:suspended)

      Success.new
    end
  end
end

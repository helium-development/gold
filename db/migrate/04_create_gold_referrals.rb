class CreateGoldReferrals < ActiveRecord::Migration[5.2]
  def change
    create_table :gold_referrals do |t|
      t.string :contact_name
      t.string :contact_email
      t.string :company
      t.string :code, index: { unique: true }, null: false
      t.integer :discount_percentage, default: 0

      t.timestamps null: false
    end

    add_column :gold_billings, :gold_referral_id, :integer
  end
end

module Gold
  # Returned when a credit cannot be issued, likely due to a restriction on
  # Shopify's side.
  module Outcomes
    CannotIssueCredit = Class.new(Failure)
  end

  # Provide a credit to a merchant.
  class IssueCreditOp
    MAX_ATTEMPTS = 3

    include Outcomes
    include Retries

    def initialize(billing, amount, reason, test = false)
      @billing = billing
      @amount = amount.to_d
      @reason = reason
      @test = test
    end

    def call
      return CannotIssueCredit.new(:amount_not_positive) if @amount <= 0
      return CannotIssueCredit.new(:missing_reason) if @reason.blank?
      return CannotIssueCredit.new(:invalid_tier) if @billing.last_selected_tier&.free?

      @billing.transition_to!(:issue_credit, amount: @amount, reason: @reason)

      # Now create the credit on Shopify's end
      credit = new_credit

      request_with_retries do
        credit.save
      end

      # Mark the credit as applied
      @billing.transition_to!(:billing)

      if credit.valid?
        Success.new
      else
        CannotIssueCredit.new(:rejected_by_shopify,
                              credit.errors.full_messages.join(", "))
      end
    end

    def new_credit
      ShopifyAPI::ApplicationCredit.new(
        description: @reason,
        amount: @amount,
        test: @test
      )
    end
  end
end

require "test_helper"
require "minitest/mock"
require "shopify_interactions"
require "gold/billing"

module Gold
  class ReferralTest < ActiveSupport::TestCase
    def setup
      @shop = Shop.create!(
        shopify_domain: "abc.myshopify.com",
        shopify_token: "3249237408230112312908"
      )
    end

    def test_invalid_duplicate_code
      Referral.create!(code: "ABC")

      refute Referral.new(code: "ABC").valid?
    end

    def test_code_format
      referral = Referral.create!(code: "little_jon")
      assert_equal "LITTLE_JON", referral.code
    end

    def test_billing_association
      referral = Referral.create!(code: "BLUEJAZZ")
      billing = Billing.create!(referral: referral, shop: @shop)

      assert_equal referral, billing.referral
      assert_equal [billing], referral.billings
    end

    def test_billing_association_nullify_on_destroy
      referral = Referral.create!(code: "SILVER")
      billing = Billing.create!(referral: referral, shop: @shop)

      assert_equal referral, billing.referral

      referral.destroy!

      refute billing.reload.referral
    end
  end
end

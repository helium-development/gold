class AddShops < ActiveRecord::Migration
  def change
    enable_extension "hstore"
    create_table :shops do |t|
      t.string   :domain,               limit: 255
      t.string   :token,                limit: 255
      t.string   :account_type,         default: "user"
      t.boolean  :billing_required,     default: true
      t.string   :plan,                 default: "basic"
      t.integer  :monthly_price,        default: 0
      t.string   :email
      t.string   :install_status,       default: "installed"
      t.string   :shop_name
      t.hstore   :details               default: {}

      t.timestamps
    end
  end
end

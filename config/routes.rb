Gold::Engine.routes.draw do
  get "/start", to: "setup#new"

  get "/terms", to: "billing#terms", as: "terms"
  put "/terms", to: "billing#process_terms"

  get "/tier", to: "billing#tier", as: "select_tier"
  put "/tier", to: "billing#select_tier"

  get "/charge", to: "billing#outstanding_charge", as: "outstanding_charge"
  get "/charge/process", to: "billing#process_charge", as: "process_charge"
  get "/charge/expired", to: "billing#expired_charge", as: "expired_charge"
  get "/charge/declined", to: "billing#declined_charge", as: "declined_charge"
  get "/charge/missing", to: "billing#missing_charge", as: "missing_charge"
  post "/charge/retry", to: "billing#retry_charge", as: "retry_charge"

  get "/suspended", to: "billing#suspended", as: "suspended"
  get "/unavailable", to: "billing#unavailable", as: "unavailable"

  get "/uninstalled", to: "billing#uninstalled", as: "uninstalled"

  get "/referral", to: "referrals#track", as: "referral"
end

Gold::AdminEngine.routes.draw do
  scope :billing do
    post '/credit', to: 'admin/billing#issue_credit', as: 'issue_credit'
    put '/suspend', to: 'admin/billing#suspend', as: 'suspend'
    put '/unsuspend', to: 'admin/billing#unsuspend', as: 'unsuspend'
    put '/reset_trial_days', to: 'admin/billing#reset_trial_days', as: 'reset_trial_days'
    put '/discount', to: 'admin/billing#apply_discount', as: 'apply_discount'
    put '/transition', to: 'admin/billing#transition', as: 'transition'
    put '/change_tier', to: 'admin/billing#change_tier', as: 'change_tier'
    put '/override_shopify_plan', to: 'admin/billing#override_shopify_plan', as: 'override_shopify_plan'
    delete '/charge/:id', to: 'admin/billing#cancel_charge', as: 'cancel_charge'
  end

  scope :referrals do
    post    "/", to: "admin/referrals#create", as: "referrals"
    patch   "/:id", to: "admin/referrals#update", as: "referral"
    post    "/billings", to: "admin/referrals#add_billing", as: "add_billing_referral"
    delete  "/:id", to: "admin/referrals#delete", as: "delete_referral"
  end
end

require "test_helper"
require "shopify_interactions"
require "minitest/mock"

module Gold
  class CleanupOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def setup
      stub_request_to_get_admin_shop
    end

    def test_cleanup_from_uninstalled
      set_up_shop_as_billing!
      transition_to! :marked_as_uninstalled
      transition_to! :uninstalled
      time_machine! 30
      verify_cleanup
    end

    def test_cleanup_from_suspended
      set_up_shop_as_billing!
      transition_to! :marked_as_suspended
      transition_to! :suspended
      time_machine! 30
      verify_cleanup
    end

    def test_cleanup_from_frozen
      set_up_shop_as_billing!
      billing.shopify_plan_override = "frozen"
      transition_to! :frozen
      time_machine! 30
      verify_cleanup
    end

    def test_cleanup_from_delinquent
      set_up_shop_as_billing!
      transition_to! :check_charge
      transition_to! :charge_missing
      transition_to! :delayed_charge, charge_id: 1000
      time_machine! 20
      transition_to! :marked_as_delinquent
      transition_to! :delinquent
      time_machine! 30
      verify_cleanup
    end

    def test_cleanup_without_on_cleanup_hook
      set_up_shop_as_billing!
      transition_to! :marked_as_uninstalled
      transition_to! :uninstalled
      time_machine! 30

      Gold.configuration.on_cleanup = nil

      outcome = CleanupOp.new(billing).call

      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
    end

    def test_cleanup_can_be_retried
      set_up_shop_as_billing!
      transition_to! :marked_as_uninstalled
      transition_to! :uninstalled
      time_machine! 30

      Gold.configuration.on_cleanup = proc { raise "Cannot cleanup right now" }

      assert_raises(RuntimeError) { CleanupOp.new(billing).call }

      # Retry with a cleanup hook that works
      Gold.configuration.on_cleanup = proc { true }

      outcome = CleanupOp.new(billing).call
      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
    ensure
      Gold.configuration.on_cleanup = nil
    end

    def test_cleanup_requires_min_of_days
      set_up_shop_as_billing!
      transition_to! :marked_as_uninstalled
      transition_to! :uninstalled
      time_machine! 5
      refute billing.transition_to(:cleanup)
    end

    private

    def verify_cleanup
      @mock_on_cleanup = MiniTest::Mock.new
      @mock_on_cleanup.expect(:call, nil, [billing])

      Gold.configuration.on_cleanup = @mock_on_cleanup

      outcome = CleanupOp.new(billing).call

      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
      @mock_on_cleanup.verify
    ensure
      Gold.configuration.on_cleanup = nil
    end
  end
end

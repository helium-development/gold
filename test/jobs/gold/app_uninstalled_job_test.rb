require "test_helper"
require "shopify_interactions"

module Gold
  class AppUninstalledJobTest < ActiveJob::TestCase
    include ShopifyInteractions

    def test_uninstall
      set_up_shop_as_billing!
      with_shopify_session do
        AppUninstalledJob.perform_now(shop_domain: shop.shopify_domain)
      end
      assert_equal :uninstalled, billing.current_state
    end
  end
end

require "test_helper"
require "minitest/mock"
require "shopify_interactions"
require "gold/billing"

module Gold
  class BillingTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def setup
      stub_request_to_get_admin_shop
    end

    def test_shop_association
      shop = Shop.create(
        shopify_domain: "xyz.myshopify.com",
        shopify_token: "76546789098765789098765890"
      )

      billing = Gold::Billing.create!(shop: shop)
      assert_equal shop.billing, billing
    end

    def test_lookup_for_domain
      domain = "xyz.myshopify.com"
      shop = Shop.create(
        shopify_domain: domain,
        shopify_token: "76546789098765789098765890"
      )

      billing = Gold::Billing.create(shop: shop)
      assert_equal billing, Gold::Billing.lookup_for_domain!(domain)
    end

    def test_lookup_for_domain_nonexistent
      assert_raises ActiveRecord::RecordNotFound do
        Gold::Billing.lookup_for_domain!("nonexistent")
      end
    end

    def test_state_machine
      billing = Gold::Billing.new
      refute_nil billing.state_machine
      assert_equal :new, billing.current_state
    end

    def test_tier
      billing = Gold::Billing.new
      assert_nil billing.tier
      basic = Tier.find(:basic)
      refute_nil basic
      billing.tier = basic
      assert_equal basic, billing.tier
      assert_equal "basic", billing.tier_id
    end

    def test_qualifies_for_tier
      billing = Gold::Billing.new
      Tier.all.each do |tier|
        assert billing.qualifies_for_tier?(tier),
               "Billing should qualify for #{tier} tier"
      end
    end

    def test_shopify_plan
      details = Object.new
      def details.plan_name
        "professional"
      end
      ShopifyAPI::Shop.stub :current, details do
        billing = Gold::Billing.new
        assert_equal "professional", billing.shopify_plan.to_s
      end
    end

    def test_shopify_plan_override
      billing = Gold::Billing.new
      billing.shopify_plan_override = "affiliate"
      assert_equal "affiliate", billing.shopify_plan.to_s
    end

    def test_trial_starts_at
      billing = Gold::Billing.new
      assert_nil billing.trial_starts_at
      time = Time.current
      billing.trial_starts_at = time
      assert_equal time, billing.trial_starts_at
    end

    def test_last_selected_tier
      set_up_shop_as_billing! :pro
      assert_equal :pro, billing.last_selected_tier.id
    end

    def test_last_selected_tier_not_present
      assert_raises(UnknownTier) { billing.last_selected_tier }
    end

    def test_last_selected_tier_not_found
      set_up_shop_as_billing! :some_fake_plan
      assert_raises(UnknownTier) { billing.last_selected_tier }
    end

    def test_last_selected_when_declined
      set_up_shop_as_billing! :basic
      billing.transition_to! :change_tier, tier_id: :pro
      billing.transition_to! :optional_charge, charge_id: "866855646"
      billing.transition_to! :optional_charge_declined
      billing.transition_to! :billing

      assert_equal :basic, billing.last_selected_tier.id
    end

    def test_shop_update_callback_converts_affiliate
      set_up_shop_as_affiliate! :basic
      billing.update(shopify_plan_override: "plus")

      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charges: []
        }.to_json)

      stub_shopify_request(:post, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charge: {
            id: "564321768767",
            name: "Xtra (50% discount)",
            status: "pending",
            confirmation_url: "https://example.myshopify.com/admin/accept"
          }
        }.to_json)

      billing.after_shop_update!

      assert_equal :delayed_charge, billing.current_state
    end

    def test_shop_update_callback_converts_frozen
      set_up_shop_as_billing! :basic
      billing.update(shopify_plan_override: "frozen")
      billing.after_shop_update!

      assert_equal :frozen, billing.current_state
    end

    def test_shop_update_callback_converts_frozen_to_billing
      set_up_shop_as_billing! :basic

      stub_shopify_request(:get, "shop.json")
        .to_return(status: 200, body: {
          shop: {
            plan_name: "frozen"
          }
        }.to_json)

      billing.transition_to! :frozen

      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charges: [{
            id: "111111111",
            name: "Advanced",
            status: "active",
            trial_days: billing.tier.trial_days,
            price: billing.tier.monthly_price,
            test: false,
            confirmation_url: "https://example.myshopify.com/admin/accept"
          }]
        }.to_json)

      stub_shopify_request(:get, "shop.json")
        .to_return(status: 200, body: {
          shop: {
            plan_name: "unlimited"
          }
        }.to_json)

      billing.after_shop_update!

      assert_equal :billing, billing.current_state
    end

    def test_app_access
      refute billing.app_access?

      set_up_shop_as_billing!
      assert billing.app_access?

      billing.transition_to!(:check_charge)
      billing.transition_to!(:charge_missing)
      billing.transition_to!(:delayed_charge, charge_id: "23456")
      billing.transition_to!(:delayed_charge_expired)
      time_machine! 7
      billing.transition_to!(:marked_as_delinquent)
      billing.transition_to!(:delinquent)
      refute billing.app_access?
    end

    def test_app_access_from_uninstalled
      billing_id = billing.id
      set_up_shop_as_billing!
      billing.transition_to!(:marked_as_uninstalled)
      billing.transition_to!(:uninstalled)
      refute billing.app_access?

      time_machine!(30)

      # Strange, but need to reload from database since it seems to be caching
      # our transitions and `billing.reload` doesn't seem to be doing the trick
      billing = Billing.find(billing_id)

      billing.transition_to!(:cleanup)
      refute billing.app_access?

      billing.transition_to!(:done)
      refute billing.app_access?

      billing.transition_to!(:reinstalled)
      assert billing.app_access?
    end
  end
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_03_223146) do

  create_table "gold_billings", force: :cascade do |t|
    t.integer "shop_id", null: false
    t.datetime "trial_starts_at"
    t.string "tier_id"
    t.string "shopify_plan_override"
    t.integer "discount_percentage", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "gold_referral_id"
    t.index ["shop_id"], name: "index_gold_billings_on_shop_id", unique: true
  end

  create_table "gold_referrals", force: :cascade do |t|
    t.string "contact_name"
    t.string "contact_email"
    t.string "company"
    t.string "code", null: false
    t.integer "discount_percentage", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_gold_referrals_on_code", unique: true
  end

  create_table "gold_transitions", force: :cascade do |t|
    t.string "to_state", null: false
    t.json "metadata", default: {}
    t.integer "sort_key", null: false
    t.integer "billing_id", null: false
    t.boolean "most_recent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["billing_id", "most_recent"], name: "index_gold_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["billing_id", "sort_key"], name: "index_gold_transitions_parent_sort", unique: true
  end

  create_table "merchants", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shops", force: :cascade do |t|
    t.string "shopify_domain", null: false
    t.string "shopify_token", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shopify_domain"], name: "index_shops_on_shopify_domain", unique: true
  end

end

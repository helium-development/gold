module Gold
  module Shop
    def self.included(base)
      base.extend(ClassMethods)
    end

    module ClassMethods
      def installed
        where(install_status: "installed")
      end

      def uninstalled
        where(install_status: "uninstalled")
      end

      def store(session)
        # Find existing shop
        shop = self.find_by(domain: session.url, token: session.token)

        # If we can't find that shop, let's do some digging
        unless shop
          # Check for a shop with that domain
          shop = self.find_by(domain: session.url)

          # If that shop has a new updated token, just update it
          if shop
            shop.token = session.token
            shop.shopify_session

            # Update the shop
            shop.update({
              shop_name: shop.shopify_current.name,
              email: shop.shopify_current.email,
              details: shop.shopify_current.attributes,
              install_status: 'installed'
            })
          else
            # Create a new shop if nothing is found
            shop = self.new({
              domain: session.url,
              token: session.token,
              account_type: 'user',
              plan: default_plan[:name],
              monthly_price: default_plan[:price],
              install_status: 'installed'
            })

            shop.shopify_session
            shop.shop_name = shop.shopify_current.name
            shop.details = shop.shopify_current.attributes

            unless shop.save
              raise shop.display_errors
            end
          end
        end

        # Return shop ID
        shop.id
      end

      def retrieve(id)
        if shop = self.where(id: id).first
          ShopifyAPI::Session.new(shop.domain, shop.token)
        end
      end
    end

    def installed?
      install_status == 'installed'
    end

    def validate_shopify_session
      unless ShopifyAPI::Base.site.to_s.include? domain
        raise "Base site is #{ShopifyAPI::Base.site.to_s},
               but domain is #{domain}"
      end
    end

    # Throttles API requests to Shopify
    def throttle_api
      validate_shopify_session

      # Don't sleep unless we're getting close to our limit
      if ShopifyAPI::credit_left < 5
        sleep API_CALL_LIMIT * ShopifyAPI::credit_used
      end
    end

    def admin?
      account_type == 'admin'
    end

    def installed?
      install_status == 'installed'
    end

    def install!
      update(install_status: "installed")
    end

    def uninstall!
      update(install_status: "uninstalled")
      cancel_subscriptions!
    end

    def shopify_session
      shop_session = ShopifyAPI::Session.new(domain, token)
      ShopifyAPI::Base.activate_session(shop_session)
    end

    # Gets information about the store
    def shopify_current
      @shopify_current ||= ShopifyAPI::Shop.current
    end

    # Are they an affiliate type account?
    def affiliate?
      details["plan_name"] == "affiliate"
    end

    # Do they own something?
    def owns?(asset)
      self == asset.shop
    end

    def display_name
      if shop_name
        shop_name
      else
        domain
      end
    end

    def webhooks
      shopify_session
      ShopifyAPI::Webhook.all
    end

    def owner_name
      details["shop_owner"]
    end

    private

    # Monthly price must be greater than 0 if billing is required
    def monthly_price_validate
      if requires_subscription?
        unless monthly_price.to_f > 0
          errors.add(:monthly_price, 'must be greater than 0')
        end
      end
    end
  end
end

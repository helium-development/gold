require "test_helper"
require "shopify_interactions"

module Gold
  class AfterAuthenticateJobTest < ActiveJob::TestCase
    include ShopifyInteractions

    def test_install
      with_shopify_session do
        AfterAuthenticateJob.perform_now(shop_domain: shop.shopify_domain)
      end
      refute_nil shop.billing
      assert_nil shop.billing.tier
      assert_equal :install, shop.billing.current_state
    end

    # Reinstall and expect to be flagged as reinstalled
    def test_reinstall
      set_up_shop_as_billing!
      transition_to! :marked_as_uninstalled
      transition_to! :uninstalled

      with_shopify_session do
        AfterAuthenticateJob.perform_now(shop_domain: shop.shopify_domain)
      end

      assert_equal :reinstalled, billing.current_state
    end

    # This job is triggered when a shop is already installed and the merchant is
    # simply logging in.
    def test_already_installed
      set_up_shop_as_billing!

      with_shopify_session do
        AfterAuthenticateJob.perform_now(shop_domain: shop.shopify_domain)
      end

      assert_equal :billing, billing.current_state
    end
  end
end

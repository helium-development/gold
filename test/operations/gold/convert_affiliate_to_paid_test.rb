require "test_helper"
require "shopify_interactions"

module Gold
  class ConvertAffiliateToPaidOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    RETURN_URL = "https://xyz.heliumdev.com/shut_up_and_take_my_money".freeze

    def reload_billing
      @billing = Billing.find_by(shop: shop)
    end

    def stub_charge_requests
      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charges: []
        }.to_json)

      stub_shopify_request(:post, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charge: {
            id: "2348763212",
            name: "Black Sheep",
            status: "pending",
            confirmation_url: "https://example.myshopify.com/admin/accept"
          }
        }.to_json)
    end

    def test_charge_requested
      set_up_shop_as_affiliate!
      self.shopify_plan = "advanced"
      reload_billing
      stub_charge_requests

      with_shopify_session do
        op = ConvertAffiliateToPaidOp.new(billing, RETURN_URL, false).call
        assert_instance_of Outcomes::PendingCharge, op
      end

      assert_equal :delayed_charge, billing.current_state
    end

    def test_affiliate_to_non_paid_fails
      set_up_shop_as_affiliate!
      self.shopify_plan = "staff"
      reload_billing

      with_shopify_session do
        op = ConvertAffiliateToPaidOp.new(billing, RETURN_URL, false)
        assert_raises(ConversionError) { op.call }
      end
    end

    def test_to_free_tier
      set_up_shop_as_affiliate!(:free)
      self.shopify_plan = "professional"
      reload_billing
      stub_charge_requests

      with_shopify_session do
        op = ConvertAffiliateToPaidOp.new(billing, RETURN_URL, false).call
        assert_instance_of Outcomes::TierApplied, op
      end

      assert_equal :billing, @billing.current_state
    end
  end
end

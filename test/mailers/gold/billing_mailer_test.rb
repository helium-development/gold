require "test_helper"
require "shopify_interactions"

module Gold
  class BillingMailerTest < ActionMailer::TestCase
    include ShopifyInteractions

    def test_suspension
      email = with_shopify_session do
        BillingMailer.suspension(billing).deliver_now
      end
      assert_includes email.from, Gold.configuration.contact_email
      assert_includes email.to, shop_properties[:email]
      assert_equal "We have suspended your access to #{Gold.configuration.app_name}",
                   email.subject
    end

    def test_delinquent
      email = with_shopify_session do
        BillingMailer.delinquent(billing).deliver_now
      end
      assert_includes email.from, Gold.configuration.contact_email
      assert_includes email.to, shop_properties[:email]
      assert_equal "We need you to approve a charge for #{Gold.configuration.app_name}",
                   email.subject
    end
  end
end

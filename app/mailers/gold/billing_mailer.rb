module Gold
  # Sends billing-related emails to merchants.
  class BillingMailer < ApplicationMailer
    layout "gold/mailer"

    # Inform a merchant about their account becoming suspended.
    def suspension(billing)
      billing.shop.with_shopify_session do
        @shop = ShopifyAPI::Shop.current
        mail(to: @shop.email,
             subject: "We have suspended your access to #{Gold.configuration.app_name}")
      end
    end

    # Inform a merchant about their non-payment.
    def delinquent(billing)
      billing.shop.with_shopify_session do
        @shop = ShopifyAPI::Shop.current
        mail(to: @shop.email,
             subject: "We need you to approve a charge for " \
                      "#{Gold.configuration.app_name}")
      end
    end

    def affiliate_to_paid(billing, confirmation_url)
      billing.shop.with_shopify_session do
        @shop = ShopifyAPI::Shop.current
        @confirmation_url = confirmation_url
        app_name = Gold.configuration.app_name

        mail(to: @shop.email,
             subject: "[Action Required] Charge needed for #{app_name}")
      end
    end
  end
end

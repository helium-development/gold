module Gold
  # A referral can be assigned to a billing record, which can be helpful for
  # tracking affiliates with discount codes
  class Referral < ApplicationRecord
    has_many :billings, dependent: :nullify,
                        foreign_key: "gold_referral_id",
                        inverse_of: "referral"

    validates :code, presence: true, uniqueness: true
    validates :discount_percentage, numericality: {
      greater_than_or_equal_to: 0,
      less_than_or_equal_to: 100
    }

    before_validation :ensure_code_format

    private

    def ensure_code_format
      self.code = code.parameterize.underscore.upcase
    end
  end
end

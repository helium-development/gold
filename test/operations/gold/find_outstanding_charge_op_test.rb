require "test_helper"
require "shopify_interactions"

module Gold
  class ResolveOutstandingChargeOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    # A sample charge ID set up for the purposes of testing this operation.
    CHARGE_ID = "1042".freeze

    def test_finds_active_sudden_charge
      set_up_shop_as_sudden_charge!
      test_finds_active_charge
    end

    def test_finds_active_delayed_charge
      set_up_shop_as_delayed_charge!
      test_finds_active_charge
    end

    def test_finds_active_charge_with_retries
      set_up_shop_as_sudden_charge!

      stub_shopify_request(:get, "recurring_application_charges/#{CHARGE_ID}.json")
        .to_return(status: 502).times(3).then
        .to_return(status: 200, body: charge_body.to_json)

      outcome = with_shopify_session do
        ResolveOutstandingChargeOp.new(billing).call
      end
      assert_instance_of Outcomes::ActiveCharge, outcome
    end

    def test_no_previous_charge
      outcome = with_shopify_session do
        ResolveOutstandingChargeOp.new(billing).call
      end
      assert_instance_of Outcomes::MissingCharge, outcome
      refute outcome.ok?
    end

    def test_finds_pending_sudden_charge
      set_up_shop_as_sudden_charge!
      test_finds_pending_charge
    end

    def test_finds_pending_delayed_charge
      set_up_shop_as_delayed_charge!
      test_finds_pending_charge
    end

    def test_finds_accepted_sudden_charge
      set_up_shop_as_sudden_charge!
      test_finds_accepted_charge
    end

    def test_finds_accepted_delayed_charge
      set_up_shop_as_delayed_charge!
      test_finds_accepted_charge
    end

    def test_finds_expired_sudden_charge
      set_up_shop_as_sudden_charge!

      stub_shopify_request(:get, "recurring_application_charges/#{CHARGE_ID}.json")
        .to_return(status: 200, body: charge_body(status: "expired").to_json)

      outcome = with_shopify_session do
        ResolveOutstandingChargeOp.new(billing).call
      end
      assert_instance_of Outcomes::ExpiredCharge, outcome
      refute outcome.ok?
      assert_equal :sudden_charge_expired, billing.current_state
    end

    def test_finds_expired_delayed_charge
      set_up_shop_as_delayed_charge!

      stub_shopify_request(:get, "recurring_application_charges/#{CHARGE_ID}.json")
        .to_return(status: 200, body: charge_body(status: "expired").to_json)

      outcome = with_shopify_session do
        ResolveOutstandingChargeOp.new(billing).call
      end
      assert_instance_of Outcomes::ExpiredCharge, outcome
      refute outcome.ok?
      assert_equal :delayed_charge_expired, billing.current_state
    end

    def test_finds_expired_optional_charge
      set_up_shop_as_optional_charge!

      stub_shopify_request(:get, "recurring_application_charges/#{CHARGE_ID}.json")
        .to_return(status: 200, body: charge_body(status: "expired").to_json)

      outcome = with_shopify_session do
        ResolveOutstandingChargeOp.new(billing).call
      end
      assert_instance_of Outcomes::ActiveCharge, outcome
      assert outcome.ok?
      assert_equal :billing, billing.current_state
    end

    def test_misses_declined_sudden_charge
      set_up_shop_as_sudden_charge!
      test_misses_declined_charge
    end

    def test_misses_declined_delayed_charge
      set_up_shop_as_delayed_charge!
      test_misses_declined_charge
    end

    private

    def set_up_shop_as_sudden_charge!
      with_shopify_session do
        transition_to! :install
        transition_to! :accepted_terms
        transition_to! :select_tier, tier_id: :pro
        transition_to! :sudden_charge, charge_id: CHARGE_ID
      end
    end

    def set_up_shop_as_delayed_charge!
      early_charge_id = "1000"
      with_shopify_session do
        transition_to! :install
        transition_to! :accepted_terms
        transition_to! :select_tier, tier_id: :basic
        transition_to! :sudden_charge, charge_id: early_charge_id
        transition_to! :sudden_charge_accepted
        transition_to! :charge_activated
        transition_to! :apply_tier, tier_id: :basic
        transition_to! :billing
        transition_to! :calculate_usage
        transition_to! :auto_upgrade_tier, tier_id: :pro
        transition_to! :delayed_charge, charge_id: CHARGE_ID
      end
    end

    def set_up_shop_as_optional_charge!
      with_shopify_session do
        transition_to! :install
        transition_to! :accepted_terms
        transition_to! :select_tier, tier_id: :free
        transition_to! :apply_free_tier, tier_id: :free
        transition_to! :billing
        transition_to! :change_tier, tier_id: :pro
        transition_to! :optional_charge, charge_id: CHARGE_ID
      end
    end

    def charge_body(charge_id: CHARGE_ID, status: "active")
      {
        recurring_application_charge: {
          id: charge_id,
          status: status,
          activated_on: "2018-07-05",
          price: "12.00",
          name: "Mordor",
          confirmation_url: "https://inert.myshopify.com/admin/confirm",
          decorated_return_url: "https://localhost:3000/?charge_id=#{charge_id}"
        }
      }
    end

    def test_finds_active_charge
      stub_shopify_request(:get, "recurring_application_charges/#{CHARGE_ID}.json")
        .to_return(status: 200, body: charge_body.to_json)

      outcome = with_shopify_session do
        ResolveOutstandingChargeOp.new(billing).call
      end
      assert_instance_of Outcomes::ActiveCharge, outcome
      assert outcome.ok?
    end

    def test_finds_pending_charge
      stub_shopify_request(:get, "recurring_application_charges/#{CHARGE_ID}.json")
        .to_return(status: 200, body: charge_body(status: "pending").to_json)

      outcome = with_shopify_session do
        ResolveOutstandingChargeOp.new(billing).call
      end
      assert_instance_of Outcomes::PendingCharge, outcome
      assert outcome.ok?
    end

    def test_finds_accepted_charge
      charge = charge_body(status: "accepted")
      stub_shopify_request(:get, "recurring_application_charges/#{CHARGE_ID}.json")
        .to_return(status: 200, body: charge.to_json)

      outcome = with_shopify_session do
        ResolveOutstandingChargeOp.new(billing).call
      end
      assert_instance_of Outcomes::AcceptedCharge, outcome
      assert outcome.ok?
      assert_equal charge[:recurring_application_charge][:decorated_return_url],
                   outcome.return_url
    end

    def test_misses_declined_charge
      stub_shopify_request(:get, "recurring_application_charges/#{CHARGE_ID}.json")
        .to_return(status: 200, body: charge_body(status: "declined").to_json)

      outcome = with_shopify_session do
        outcome = ResolveOutstandingChargeOp.new(billing).call
      end
      assert_instance_of Outcomes::MissingCharge, outcome
      refute outcome.ok?
    end
  end
end

# rubocop:disable Metrics/LineLength

# Configure Rails Environment
ENV["RAILS_ENV"] = "test"

# NOTE(andrew): Load the development gems, since there is not a way to say in
# the gemspec that dev dependencies should also be used for tests. Before this,
# `rails test` was failing to load some shopify_app dependencies in the
# test/dummy/config/initializers.
ENV["RAILS_GROUPS"] = "development"

require_relative "../test/dummy/config/environment"
ActiveRecord::Migrator.migrations_paths = [File.expand_path("../test/dummy/db/migrate", __dir__)]
require "rails/test_help"

# Filter out Minitest backtrace while allowing backtrace from other libraries
# to be shown.
Minitest.backtrace_filter = Minitest::BacktraceFilter.new

# Load fixtures from the engine
if ActiveSupport::TestCase.respond_to?(:fixture_path=)
  ActiveSupport::TestCase.fixture_path = File.expand_path("fixtures", __dir__)
  ActionDispatch::IntegrationTest.fixture_path = ActiveSupport::TestCase.fixture_path
  ActiveSupport::TestCase.file_fixture_path = ActiveSupport::TestCase.fixture_path + "/files"
  ActiveSupport::TestCase.fixtures :all
end

# Use WebMock for inspecting and manipulating HTTP requests
require "webmock/minitest"

# Start recording state machine coverage against Gold::Machine
Gold::Machine.include(Gold::Coverage)

Rails.application.config.active_job.queue_adapter = :inline

# Save coverage results for later inspection
Minitest.after_run do
  path = Rails.root.join("tmp", "gold_machine_coverage.svg")
  Gold::Machine.coverage.to_svg(path)
  puts
  puts "Gold::Machine coverage saved to #{path}"
rescue StandardError => e
  puts "Could not generate a coverage diagram for Gold::Machine: #{e}"
end

# rubocop:enable Metrics/LineLength

Gold.configure do |config|
  config.test_charge = false

  config.shop_class = "Shop"
  config.shop_domain_attribute = "shopify_domain"
  config.app_name = "Example App"
  config.contact_email = "support@example.com"

  # Credentials to login to the backend
  config.admin_credentials = {
    user: ENV["GOLD_ADMIN_USER"],
    password: ENV["GOLD_ADMIN_PASSWORD"]
  }

  config.plan_comparison_url = "http://localhost:3000/plans"
  config.shopify_api_version = "2019-04"
  config.days_until_delinquent = 7

  config.on_install = proc {
    puts "Installing Gold..."
  }

  config.on_terms = proc {
    puts "Accepted terms from Gold..."
  }

  config.on_apply_tier = proc { |billing, tier_id|
    puts "Applied tier..."
  }

  config.on_activate_charge = proc { |billing, charge|
    puts "Activated charge..."
  }

  config.on_uninstall = proc {
    puts "Uninstalling Gold..."
  }

  config.on_cleanup = proc { |billing|
    puts "Cleaning up shop"
  }
end

Gold::Engine.configure do |config|
  config.routes.default_url_options = Rails.application.routes.default_url_options
end








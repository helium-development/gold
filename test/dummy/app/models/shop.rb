class Shop < ApplicationRecord
  include ShopifyApp::ShopSessionStorage
  include Gold::Concerns::Gilded

  def api_version
    "2019-04"
  end

  def shopify_plan_name
    with_shopify_session do
      ShopifyAPI::Shop.current.plan_name
    end
  end

  def qualifies_for_tier?(_tier)
    true
  end
end

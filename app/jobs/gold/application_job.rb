module Gold
  # Base for all Gold-related background jobs to build upon.
  class ApplicationJob < ActiveJob::Base
    include Engine.routes.url_helpers

    queue_as :gold

    # Automatically retry jobs that encountered a deadlock
    # retry_on ActiveRecord::Deadlocked

    # Most jobs are safe to ignore if the underlying records are no longer available
    # discard_on ActiveJob::DeserializationError

    protected

    def default_url_options
      Rails.application.config.default_url_options
    end
  end
end

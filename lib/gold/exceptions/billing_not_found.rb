module Gold
  module Exceptions
    class BillingNotFound < ActiveRecord::RecordNotFound; end
  end
end

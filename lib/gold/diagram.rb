require "graphviz"

module Gold
  # Renders graphs of `Statesman::Machine`s.
  class Diagram
    # Prepares to render a diagram from a `Statesman::Machine` class.
    def initialize(machine, node_callback: nil, edge_callback: nil)
      @machine = machine
      @node_callback = node_callback
      @edge_callback = edge_callback
    end

    # Renders a diagram and saves it to a SVG stored at `output`.
    def render(output)
      g = GraphViz.new(@machine.to_s, type: :digraph)

      # Mark the start of the state machine with a "start" label and arrow to the
      # initial state.
      g.add_node("start", shape: :plaintext)
      g.add_edge("start", @machine.initial_state)

      # Add all of the states as nodes.
      @machine.states.each do |state|
        node = g.add_node(state)
        @node_callback&.call(node)
      end

      # Add edges between connected states.
      @machine.successors.each do |state, successors|
        successors.each do |successor|
          edge = g.add_edge(state, successor)
          @edge_callback&.call(edge)
        end
      end

      # Save the output to a file.
      g.output(svg: output)
    end
  end
end

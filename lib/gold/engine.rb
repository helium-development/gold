module Gold
  # Brings the Gold to your app. Mount this in your config/routes.rb.
  class Engine < ::Rails::Engine
    isolate_namespace Gold
    config.autoload_paths << File.expand_path("../../lib", __dir__)
    engine_name "gold_engine"

    initializer "gold.assets.precompile" do |app|
      app.config.assets.precompile += %w[
        gold/billing.css
        gold/billing.js
        shopify/polaris.css
      ]
    end
  end
end

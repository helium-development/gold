module Gold
  # Uninstalls the app for the merchant.
  class AppUninstalledJob < Gold::ApplicationJob
    def perform(shop_domain:, webhook: nil) # rubocop:disable Lint/UnusedMethodArgument
      shop = Gold.shop_class.find_by!(Gold.shop_domain_attribute => shop_domain)
      billing = Gold::Billing.find_by!(shop: shop)
      UninstallOp.new(billing).call
    end
  end
end

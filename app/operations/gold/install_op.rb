module Gold
  InstallFailed = Class.new(Outcomes::Failure)

  # Invoked when a shop has installed (or reinstalled) this app.
  class InstallOp
    include Outcomes

    def initialize(billing)
      @billing = billing
    end

    def call
      # Transition to reinstalled, if possible
      @billing.transition_to(:reinstalled)

      case @billing.current_state
      when :new, :reinstalled
        # Transition to install, if possible
        @billing.transition_to(:install)
        Gold.configuration.on_install&.call(@billing)
      when :frozen
        return CheckChargeOp.new(@billing).call
      when :cleanup
        Gold.logger.warn("[#{@billing.id}] Tried installing?" \
                         "'#{@billing.shop.shopify_domain}', but on cleanup")
        return InstallFailed.new(:cleanup_in_progress)
      end

      Success.new
    end
  end
end

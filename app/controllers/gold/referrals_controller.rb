module Gold
  # Handles referral tracking
  class ReferralsController < ApplicationController
    def track
      referral = Referral.find_by(code: params[:code])

      cookies[:gold_referral_id] = { value: referral.id, expires: 30.days } if referral

      redirect_to Gold.configuration.app_listing_url
    end
  end
end

require "test_helper"
require "yaml"
require "minitest/mock"

module Gold
  class TierTest < ActiveSupport::TestCase
    def with_tiers(yaml, &block)
      Tier.reset!
      Tier.stub :load_from_yaml, YAML.safe_load(yaml), &block
      Tier.reset!
    end

    def test_no_tiers
      with_tiers "---" do
        assert Tier.all.empty?
      end
    end

    # Ensure that a tier fails to parse if it is not a mapping.
    def test_invalid_tier
      with_tiers "- invalid" do
        assert_raises(ArgumentError) { Tier.all }
      end
    end

    # Ensure that a tier has an ID set.
    def test_tier_without_id
      with_tiers '- name: "My Tier"' do
        assert_raises(ArgumentError) { Tier.all }
      end
    end

    # Define the simplest tier and see that it parses correctly.
    def test_single_minimal_tier
      with_tiers %(
        - id: minimal
          name: "Minimal Tier"
      ) do
        assert_equal 1, Tier.all.size
        tier = Tier.all.first
        assert_equal :minimal, tier.id
        assert_equal "Minimal Tier", tier.name
        assert tier.visible?
        refute tier.locked?
        assert_equal 0, tier.trial_days
        assert_equal 0, tier.monthly_price
        refute tier.free?
        assert tier.features.empty?
        assert tier.qualifications.empty?
      end
    end

    # Set every option for a tier and see that it parses correctly.
    def test_single_maximal_tier
      with_tiers %(
        - id: maximal
          name: "Maximal Tier"
          visible: false
          locked: true
          pricing:
            trial_days: 14
            monthly: "12.99"
          features:
            api_access: true
          qualifications:
            max_customers: 10
      ) do
        assert_equal 1, Tier.all.size
        tier = Tier.all.first
        assert_equal :maximal, tier.id
        assert_equal "Maximal Tier", tier.name
        refute tier.visible?
        assert tier.locked?
        assert_equal 14, tier.trial_days
        assert_equal BigDecimal("12.99"), tier.monthly_price
        refute tier.free?
        assert_equal 1, tier.features.size
        assert tier.features[:api_access]
        assert_equal 1, tier.qualifications.size
        assert_equal 10, tier.qualifications[:max_customers]
      end
    end

    # Ensure a free tier can be parsed correctly.
    def test_free_tier
      with_tiers %(
        - id: free
          pricing:
            free: true
      ) do
        assert_equal 1, Tier.all.size
        tier = Tier.all.first
        assert tier.free?
      end
    end

    # Ensure that multiple tiers can be parsed correctly.
    def test_multiple_tiers
      with_tiers %(
        - id: free
        - id: basic
        - id: advanced
      ) do
        assert_equal 3, Tier.all.size
        assert_equal :free, Tier.all[0].id
        assert_equal :basic, Tier.all[1].id
        assert_equal :advanced, Tier.all[2].id
      end
    end

    # Ensure that visible tiers are distinct from all tiers.
    def test_visible
      with_tiers %(
        - id: free
        - id: basic
          visible: false
        - id: advanced
      ) do
        assert_equal 2, Tier.visible.size
        assert_equal %i[free advanced], Tier.visible.map(&:id)
      end
    end

    # Ensure that +find+ finds the tier with the same name.
    def test_find
      with_tiers %(
        - id: free
        - id: basic
        - id: advanced
      ) do
        assert_equal :free, Tier.find("free").id
        assert_equal :advanced, Tier.find(:advanced).id
      end
    end

    # If +find+ is given nil as the ID, it returns nil.
    def test_find_nil_id
      with_tiers %(
        - id: free
        - id: basic
        - id: advanced
      ) do
        assert_nil Tier.find(nil)
      end
    end

    # If no tier exists for a particular ID, +find+ returns nil.
    def test_find_missing_tier
      with_tiers %(
        - id: free
        - id: basic
        - id: advanced
      ) do
        assert_nil Tier.find(:missing)
      end
    end

    def test_inherits_from_parent
      with_tiers %(
        - id: basic
          pricing:
            monthly: "5.99"
          features:
            self_destruction: true
            zombies: "ultra"
          qualifications:
            global_domain: true
        - id: basic_legacy
          parent_id: basic
          features:
            manual_override: true
      ) do
        tier = Tier.find(:basic_legacy)

        assert_equal :basic, tier.parent.id
        assert tier.features[:self_destruction]
        assert tier.features[:manual_override]
        assert tier.qualifications[:global_domain]
      end
    end
  end
end

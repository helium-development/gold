require "test_helper"
require "minitest/mock"
require "shopify_interactions"

module Gold
  class BillingMigratorTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def setup
      @shop = Shop.create(
        shopify_domain: "xyz.myshopify.com",
        shopify_token: "76546789098765789098765890"
      )

      stub_request_to_get_admin_shop
    end

    def test_finds_matching_tier
      with_tiers %(
        - id: totally_free
          free: true
        - id: basic
          name: "Basic"
      ) do
        stub_charge(
          name: "CyberNet",
          price: "13.66",
          status: "active"
        )

        @shop.with_shopify_session do
          migrator = Gold::BillingMigrator.new(@shop, "basic")
          migrator.lookup_tier!
          assert_equal :basic, migrator.tier.id
        end
      end
    end

    def test_tier_matches_without_charge
      with_tiers %(
        - id: advanced
          name: "Advanced"
      ) do
        migrator = Gold::BillingMigrator.new(@shop, "advanced")
        stub_charges([])

        @shop.with_shopify_session do
          migrator.lookup_tier!
          assert_equal :advanced, migrator.tier.id
        end
      end
    end

    def test_finds_matching_legacy_tier
      with_tiers %(
        - id: basic
          name: "Basic"
          pricing:
            monthly: "12.99"
          features:
            gold_digging: true
        - id: pro
          name: "Pro"
          pricing:
            monthly: "20.00"
        - id: basic_legacy
          parent_id: basic
          name: "Basic (legacy)"
          pricing:
            monthly: "5.00"
      ) do
        stub_charge(
          name: "Green Beans",
          price: "5.00",
          status: "active"
        )

        @shop.with_shopify_session do
          migrator = Gold::BillingMigrator.new(@shop, "basic")
          migrator.lookup_tier!
          assert_equal :basic_legacy, migrator.tier.id
          assert migrator.tier.features[:gold_digging]
        end
      end
    end

    def test_falls_back_to_tier
      with_tiers %(
        - id: basic
          name: "Basic"
          pricing:
            monthly: "12.99"
          features:
            gold_digging: true
        - id: basic_legacy
          parent_id: basic
          name: "Basic (legacy)"
          pricing:
            monthly: "5.00"
      ) do
        stub_charge(
          name: "Worms",
          price: "6.78",
          status: "active"
        )

        @shop.with_shopify_session do
          migrator = Gold::BillingMigrator.new(@shop, "basic")
          migrator.lookup_tier!
          assert_equal :basic, migrator.tier.id
        end
      end
    end

    def test_no_tier_raises_error
      with_tiers %(
        - id: xtra
          name: "Basic"
      ) do
        migrator = Gold::BillingMigrator.new(@shop, "basic")
        stub_charges([])

        assert_raises Outcomes::TierNotFound do
          @shop.with_shopify_session do
            migrator.lookup_tier!
          end
        end
      end
    end

    def test_existing_shop_halts
      # Create a billing record first
      Billing.create!(shop: @shop)

      migrator = Gold::BillingMigrator.new(@shop, "basic")
      refute migrator.migrate!
    end

    # Normal shops without a charge should be faced with a sudden charge
    def test_no_charge_is_sudden_charge
      with_tiers %(
        - id: max
          name: "Max"
          pricing:
            monthly: "18.22"
      ) do
        trial_starts_at = 4.days.ago
        migrator = Gold::BillingMigrator.new(@shop, "max", trial_starts_at)

        stub_charges([])

        stub_shopify_request(:post, "recurring_application_charges.json")
          .to_return(status: 200, body: {
            recurring_application_charge: {
              name: "Max",
              price: "18.22",
              confirmation_url: "https://gold.digger/confirm"
            }
          }.to_json)

        @shop.with_shopify_session do
          migrator.migrate!
          assert_equal trial_starts_at, @shop.billing.trial_starts_at
          assert_equal :sudden_charge, @shop.billing.current_state
        end
      end
    end

    def test_charge_with_no_matching_price_is_sudden_charge
      with_tiers %(
        - id: joe
          name: "Joe"
          pricing:
            monthly: "10.00"
      ) do
        migrator = Gold::BillingMigrator.new(@shop, "joe")

        stub_charge(
          name: "Looks Like Joe",
          price: "10.01",
          status: "active",
          confirmation_url: "https://gold.digger/confirm"
        )

        stub_shopify_request(:post, "recurring_application_charges.json")
          .to_return(status: 200, body: {
            recurring_application_charge: {
              name: "Joe",
              price: "10.00",
              confirmation_url: "https://gold.digger/confirm"
            }
          }.to_json)

        @shop.with_shopify_session do
          migrator.migrate!
          assert_equal :sudden_charge, @shop.billing.current_state
        end
      end
    end

    def test_active_matching_charge_is_billing
      with_tiers %(
        - id: pro
          name: "Pro"
          locked: true
          pricing:
            monthly: "20.00"
      ) do
        stub_charge(
          price: "20.00",
          name: "Pro"
        )

        migrator = Gold::BillingMigrator.new(@shop, "pro")

        charge = {
          id: "928403823".to_i,
          name: "Pro",
          price: "20.00",
          status: "active",
          test: false,
          confirmation_url: "https://gold.digger/confirm"
        }

        stub_charge(charge)

        stub_shopify_request(:get, "recurring_application_charges/#{charge[:id]}.json")
          .to_return(status: 200, body: {
            recurring_application_charge: charge
          }.to_json)

        @shop.with_shopify_session do
          migrator.migrate!
          assert_equal :billing, @shop.billing.current_state
        end
      end
    end

    private

    def stub_charge(charge)
      stub_charges([charge])
    end

    def stub_charges(charges)
      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charges: charges
        }.to_json)
    end
  end
end

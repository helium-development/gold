require "gold/billing_migrator"
require "gold/configuration"
require "gold/engine"
require "gold/outcomes"
require "statesman"

# Configure Statesman to store transitions in the database.
Statesman.configure do
  storage_adapter Statesman::Adapters::ActiveRecord
end

# This is the main module where configuration can be set.
module Gold
  class << self
    attr_accessor :configuration

    delegate :shop_class,
             :shop_domain_attribute,
             :logger,
             to: :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end
end

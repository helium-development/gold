require "test_helper"
require "shopify_interactions"

module Gold
  class AcceptOrDeclineChargeOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def test_accepted_terms_succeeds
      transition_to! :install
      op = AcceptOrDeclineTermsOp.new(billing, true).call
      assert_instance_of Outcomes::AcceptedTerms, op
      assert op.ok?
      assert_equal :accepted_terms, billing.current_state
    end

    def test_accepted_terms_fails
      transition_to! :install
      op = AcceptOrDeclineTermsOp.new(billing, false).call
      assert_instance_of Outcomes::DeclinedTerms, op
      refute op.ok?
      assert_equal :install, billing.current_state
    end
  end
end

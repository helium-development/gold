class HomeController < ApplicationController
  include Gold::Concerns::MerchantFacing

  before_action :confront_mandatory_billing_action

  def index
  end
end

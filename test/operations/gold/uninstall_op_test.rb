require "test_helper"
require "shopify_interactions"
require "minitest/mock"

module Gold
  class UninstallOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def test_uninstall_as_billing
      set_up_shop_as_billing!
      uninstall_and_verify!
    end

    def test_uninstall_as_affiliate
      self.shopify_plan = "affiliate"
      with_shopify_session do
        transition_to! :install
        transition_to! :accepted_terms
        transition_to! :select_tier, tier_id: Tier.find(:basic).id
        transition_to! :affiliate
      end
      uninstall_and_verify!
    end

    def test_uninstall_as_staff
      self.shopify_plan = "staff"
      with_shopify_session do
        transition_to! :install
        transition_to! :accepted_terms
        transition_to! :select_tier, tier_id: Tier.find(:basic).id
        transition_to! :staff
      end
      uninstall_and_verify!
    end

    def test_uninstall_as_suspended_fails
      set_up_shop_as_billing!
      with_shopify_session do
        transition_to! :marked_as_suspended
        transition_to! :suspended
      end

      assert_raises Statesman::TransitionFailedError do
        with_shopify_session { UninstallOp.new(billing).call }
      end
    end

    def test_uninstall_without_on_uninstall_hook
      set_up_shop_as_billing!

      Gold.configuration.on_uninstall = nil

      assert_nil Gold.configuration.on_uninstall
      outcome = with_shopify_session { UninstallOp.new(billing).call }

      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
    end

    def test_uninstall_can_be_retried
      set_up_shop_as_billing!

      Gold.configuration.on_uninstall = proc { raise "Can't uninstall right now" }

      assert_raises(RuntimeError) do
        with_shopify_session { UninstallOp.new(billing).call }
      end

      # Retry with an uninstall hook that works
      Gold.configuration.on_uninstall = proc { true }

      outcome = with_shopify_session { UninstallOp.new(billing).call }
      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
    ensure
      Gold.configuration.on_uninstall = nil
    end

    private

    def uninstall_and_verify!
      @mock_on_uninstall = MiniTest::Mock.new
      @mock_on_uninstall.expect(:call, nil, [billing])

      Gold.configuration.on_uninstall = @mock_on_uninstall

      outcome = with_shopify_session { UninstallOp.new(billing).call }

      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
      assert_equal :uninstalled, billing.current_state
      @mock_on_uninstall.verify
    ensure
      Gold.configuration.on_uninstall = nil
    end
  end
end

module Gold
  # Invoked when Shopify suspends a merchant. This is the opposite of `ThawOp`.
  class FreezeOp
    include Outcomes

    def initialize(billing)
      @billing = billing
    end

    def call
      @billing.transition_to_or_stay_in!(:frozen)
      Success.new
    end
  end
end

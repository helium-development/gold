module Gold
  module Outcomes
    AcceptedTerms  = Class.new(Success)
    DeclinedTerms  = Class.new(Failure)
  end

  # Mark a store as having accepted/denied our Terms of Service.
  class AcceptOrDeclineTermsOp
    include Outcomes

    def initialize(billing, accepted)
      @billing = billing
      @accepted = accepted
    end

    def call
      if @accepted
        @billing.transition_to!(:accepted_terms)

        return AcceptedTerms.new
      end

      DeclinedTerms.new
    end
  end
end

module Gold
  class UnknownTier < StandardError; end

  # Class that tracks all billing details. Intended to be associated with a shop
  # class that the app uses
  class Billing < ApplicationRecord
    include Statesman::Adapters::ActiveRecordQueries

    class << self
      def transition_class
        Gold::Transition
      end

      def initial_state
        :new
      end

      # Finds a billing instance for a particular shop, using the shop's domain
      # as the key to look it up. Because this is not stored in the Billing
      # model, a join against the shop association is required.
      def lookup_for_domain!(domain)
        # This is a little bit hard to follow, but `#joins` uses the name of the
        # association as defined below as "shop" and `#where` expects the name
        # of the associated table, which we can query from the class using
        # `#table_name`.
        joins(:shop)
          .where(
            Gold.shop_class.table_name => {
              Gold.shop_domain_attribute => domain
            }
          )
          .first!
      end
    end

    # rubocop:disable Rails/ReflectionClassName
    belongs_to :shop, class_name: Gold.shop_class.to_s
    belongs_to :referral, class_name: "Gold::Referral",
                          foreign_key: "gold_referral_id",
                          inverse_of: :billings,
                          optional: true
    has_many :transitions, class_name: transition_class.to_s,
                           autosave: false,
                           dependent: :destroy
    # rubocop:enable Rails/ReflectionClassName

    def state_machine
      transition_class = self.class.transition_class
      @state_machine ||= Machine.new(self, transition_class: transition_class,
                                           association_name: :transitions)
    end

    delegate :can_transition_to?,
             :transition_to!,
             :transition_to,
             :transition_to_or_stay_in!,
             :transition_to_or_stay_in,
             :current_state,
             to: :state_machine

    def tier
      @tier ||= Tier.find(tier_id)
    end

    def tier=(tier)
      @tier = tier
      self[:tier_id] = tier.id
    end

    def last_selected_tier
      @last_selected_tier ||= find_last_selected_tier
    end

    # Returns the Shopify plan for this shop.
    def shopify_plan
      if shopify_plan_override
        ShopifyPlan.new(shopify_plan_override)
      elsif shop
        ShopifyPlan.new(shop.shopify_plan_name)
      else
        ShopifyPlan.new(ShopifyAPI::Shop.current.plan_name)
      end
    end

    def calculate_trial_days(tier, now = Time.current)
      starts_at = trial_starts_at || now
      trial_ends_at = starts_at.advance(days: tier.trial_days)
      trial_period = trial_ends_at - now
      if trial_period > 0
        (trial_period / 1.day).ceil
      else
        0
      end
    end

    def qualifies_for_tier?(tier)
      shop ? shop.qualifies_for_tier?(tier) : true
    end

    def trial_days_left
      calculate_trial_days(tier)
    end

    def calculate_price(tier)
      tier.monthly_price * (100 - discount_percentage) / 100
    end

    # Provides a way to let Gold know that the shop has changed. Your app should call this
    # whenever a new shop update webhook is received or is manually overridden
    def after_shop_update!
      return_url = Engine.routes.url_helpers.process_charge_url

      Gold.logger.info("Received shop update from '#{shop.shopify_domain}'")

      shop.with_shopify_session do
        case current_state
        when :affiliate
          if shopify_plan.paying?
            # If an affiliate has converted, change them to a paid plan
            ConvertAffiliateToPaidOp.new(self, return_url).call
          end
        when :billing
          # If their plan has been frozen, move to frozen state
          FreezeOp.new(self).call if can_transition_to?(:frozen)
        when :frozen
          CheckChargeOp.new(self).call unless shopify_plan.frozen?
        end
      end
    end

    # Shops within these states should not get access to the app or services
    def app_access?
      blacklist = %i[
        new
        accepted_terms
        select_tier
        sudden_charge
        sudden_charge_declined
        sudden_charge_expired
        marked_as_suspended
        suspended
        frozen
        delinquent
        marked_as_uninstalled
        uninstalled
        cleanup
        done
      ]

      !blacklist.include?(current_state)
    end

    protected

    # Returns the Shopify-specific properties for the current shop.
    def shopify_properties
      @shopify_properties ||= ShopifyAPI::Shop.current
    end

    def find_last_selected_tier
      transition = state_machine.last_transition_to(
        :select_tier,
        :change_tier,
        :optional_charge_declined,
        :auto_upgrade_tier
      )

      raise UnknownTier.new, "Billing '#{id}' never selected a tier" unless transition

      selected_tier = if transition.to_state.to_sym == :optional_charge_declined
                        # If the latest transition is a declined charge, fallback to
                        # the original tier
                        tier_id
                      else
                        transition.metadata["tier_id"]
                      end

      unless (last_tier = Tier.find(selected_tier))
        raise UnknownTier.new, "Billing '#{id}' tier '#{tier_id}' is undefined"
      end

      last_tier
    end
  end
end

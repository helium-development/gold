module Gold
  module Admin
    # Main controller for Gold to be used in app
    class BillingController < AdminController
      around_action :shopify_session

      rescue_from Statesman::TransitionFailedError,
                  Statesman::GuardFailedError,
                  Gold::Exceptions::MetadataMissing do |e|
        flash[:danger] = e.message
        go_back
      end

      def transition
        transitions = [params[:to]].flatten

        transitions.each do |t|
          billing.transition_to!(t, metadata)
        end

        flash[:success] = "Transitioned to '#{transitions.join(',')}'"
        go_back
      end

      # rubocop:disable Metrics/LineLength:Length
      def change_tier
        tier = Tier.find(params[:tier])

        outcome = SelectTierOp.new(billing, tier).call

        if outcome.is_a?(Outcomes::TierApplied)
          flash[:success] = "Changed billing to '#{tier.name}'"
        elsif outcome.ok?
          charge_outcome = ChargeOp.new(billing, process_charge_url).call

          if charge_outcome.is_a?(Outcomes::ActiveCharge)
            accept_or_decline_outcome = AcceptOrDeclineChargeOp.new(billing, charge_outcome.charge_id).call

            if accept_or_decline_outcome.ok?
              apply_tier_outcome = ApplyTierOp.new(billing).call

              if apply_tier_outcome.ok? # rubocop:disable Metrics/BlockNesting
                flash[:success] = "Changed billing to '#{tier.name}', no merchant action required"
              else
                flash[:danger] = "Failed to apply tier because '#{apply_tier_outcome.reason}'"
              end
            else
              flash[:danger] = "Failed to accept or decline charge because '#{accept_or_decline_outcome.reason}'"
            end
          elsif charge_outcome.is_a?(Outcomes::ChargeNotNeeded)
            flash[:success] = "Applied free tier '#{tier.name}', no merchant action required. You may need to cancel their current charge."
          else
            flash[:warning] = "Changing tier, but merchant action is required before tier is applied"
          end
        else
          flash[:danger] = "Failed to apply tier because '#{outcome.reason}'"
        end

        go_back
      end
      # rubocop:enable Metrics/LineLength:Length

      # Issue a credit to a shop
      def issue_credit
        outcome = IssueCreditOp.new(billing, params[:amount], params[:reason]).call

        if outcome.ok?
          flash["success"] = "Issued credit"
        else
          flash["danger"] = "Failed to issue credit because '#{outcome.message}'"
        end

        go_back
      end

      def cancel_charge
        begin
          charge = ShopifyAPI::RecurringApplicationCharge.find(params[:charge_id])
          charge.cancel
          flash["success"] = "Cancelled charge"
        rescue ActiveResource::ResourceNotFound
          flash["warning"] = "That charge was not found for '#{ShopifyAPI::Base.site}'"
        end

        go_back
      end

      # Suspend a shop
      def suspend
        if params[:confirm_suspend].blank?
          flash[:danger] = "Please confirm you want to suspend this account"
          go_back && return
        end

        SuspendOp.new(billing).call
        flash[:success] = "Account suspended"
        go_back
      end

      # Unsuspend a shop
      def unsuspend
        outcome = UnsuspendOp.new(billing).call

        Gold::ChargeOp.new(billing, process_charge_url).call unless outcome.ok?

        flash[:success] = "Account unsuspended, charge is '#{outcome}'"
        go_back
      end

      def apply_discount
        outcome = ApplyDiscountOp.new(billing,
                                      params[:percentage].to_i,
                                      process_charge_url).call

        if outcome.is_a?(Outcomes::PendingCharge)
          flash[:warning] = "Discount applied, merchant must reauthorize the charge"
        elsif outcome.ok?
          flash[:success] = "Discount updated, will be used for future charges"
        else
          flash[:danger] = "Failed to apply discount because '#{outcome.message}'"
        end

        go_back
      end

      def override_shopify_plan
        plan = params[:plan].presence
        if billing.update(shopify_plan_override: plan)
          flash[:success] = if plan
                              "Changed Shopify plan to '#{plan}'"
                            else
                              "Removed Shopify plan override"
                            end
        else
          flash[:danger] = "Failed to save shop"
        end

        go_back
      end

      def reset_trial_days
        case params[:trial_action]
        when "reset"
          if billing.update(trial_starts_at: Time.current)
            flash[:success] = "New trial starts... now! You might need to" \
                              "cancel their charge first."
          else
            flash[:danger] = "Failed to reset trial days"
          end
        when "clear"
          if billing.update(trial_starts_at: nil)
            flash[:success] = "Trial started over. The merchant will be faced" \
                              "with a new charge upon login."
          else
            flash[:danger] = "Failed to clear trial days"
          end
        else
          flash[:danger] = "Don't know what to do with" \
                          " '#{params[:trial_action]}' trial action"
        end

        go_back
      end

      private

      def shopify_session
        billing.shop.with_shopify_session do
          yield
        end
      end

      def billing
        @billing ||= Billing.find(params[:id])
      end

      def metadata
        md = {
          applied_by: "admin"
        }

        if params.dig(:metadata, :key).present?
          md[params[:metadata][:key]] = params[:metadata][:value]
        end
        md
      end

      def go_back
        redirect_back(fallback_location: main_app.gold_admin_engine_path)
      end
    end
  end
end

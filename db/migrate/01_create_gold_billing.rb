class CreateGoldBilling < ActiveRecord::Migration[5.2]
  def change
    create_table :gold_billings do |t|
      t.integer :shop_id, null: false, index: { unique: true }
      t.timestamp :trial_starts_at
      t.string :tier_id
      t.string :shopify_plan_override
      t.integer :discount_percentage, null: false, default: 0
      t.timestamps null: false
    end
  end
end

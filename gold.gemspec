$LOAD_PATH.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "gold/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "shopify-gold"
  s.version     = Gold::VERSION
  s.authors     = ["Andrew Smith", "Nick Mealey"]
  s.email       = ["andrew@heliumdev.com", "nick@heliumdev.com"]
  s.homepage    = "https://gitlab.com/helium-development/infrastructure/gold"
  s.summary     = "Helium's approach to billing for Shopify apps"
  s.description = <<~DESC
    Gold is Helium's approach to billing for Shopify apps. It provides a
    framework to build Rails apps upon.
  DESC

  s.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.md"]

  s.required_ruby_version = "~> 2.5"

  s.add_dependency "rails", "~> 6"
  s.add_dependency "sass-rails"
  s.add_dependency "shopify_app", "> 16"
  s.add_dependency "statesman", "~> 3"

  s.add_development_dependency "dotenv-rails", "~> 2.7"
  s.add_development_dependency "faker", "~> 1.9"
  s.add_development_dependency "minitest-skip", "~> 0.0"
  s.add_development_dependency "rubocop", ">= 0.74"
  s.add_development_dependency "rubocop-rails", "~> 2.3.2"
  s.add_development_dependency "ruby-graphviz", "~> 1.2"

  # List shopify_app as a development dependency too so that `test/dummy` can
  # use it properly
  s.add_development_dependency "shopify_app", "> 16"
  s.add_development_dependency "sqlite3", "~> 1.3"
  s.add_development_dependency "webmock", "~> 3"
end

require "bigdecimal"
require "yaml"

module Gold
  # A tier is level of service that this app offer merchants. Tiers are named,
  # have a series of available features, and have pricing data. Tiers are
  # configured in an app's config/tiers.yml file.
  class Tier
    CONFIG_FILE = Rails.root.join("config", "tiers.yml")

    class << self
      def load_from_yaml
        YAML.load_file(CONFIG_FILE)
      end

      # Returns the defined tiers.
      def all
        @all ||= (load_from_yaml || []).map { |tier| Tier.new(tier) }
      end

      # Returns only the visible tiers (ones that customers should see on a
      # pricing comparision page).
      def visible
        all.find_all(&:visible?)
      end

      # Returns the tier by ID or nil if that tier cannot be found.
      def find(id)
        return nil if id.nil?

        all.find { |tier| tier.id == id.to_sym }
      end

      # Exposed for testing to allow clearing the cached tiers. Not necessary
      # for normal application use.
      def reset!
        @all = nil
      end
    end

    attr_reader :id,
                :name,
                :description,
                :trial_days,
                :monthly_price

    def initialize(options = {})
      raise ArgumentError, "Cannot parse tier: #{options}" unless options.is_a?(Hash)
      raise ArgumentError, "ID must be provided for a tier" unless options["id"]

      @id = options["id"].to_sym
      @parent_id = options["parent_id"]
      @name = options["name"]
      @description = options["description"]
      @locked = options.fetch("locked", false)
      @visible = options.fetch("visible", true)
      @trial_days = options.dig("pricing", "trial_days") || 0
      @monthly_price = BigDecimal(options.dig("pricing", "monthly") || 0)
      @free = options.dig("pricing", "free") || false
      @qualifications = (options["qualifications"] || {}).with_indifferent_access
      @features = (options["features"] || {}).with_indifferent_access
    end

    # Whether the tier has been locked and new shops can no longer user it
    # without an admin assigning it to their shop.
    def locked?
      @locked
    end

    # Whether the tier should show up in a list of available options.
    def visible?
      @visible
    end

    # Whether the tier is completely free for shops to use. This is different
    # than a tier that has a zero monthly price but usage-based charges.
    def free?
      @free
    end

    def qualifications
      parent ? parent.qualifications.merge(@qualifications) : @qualifications
    end

    def features
      parent ? parent.features.merge(@features) : @features
    end

    def parent
      @parent_id ? self.class.find(@parent_id.to_sym) : nil
    end

    def top_id
      parent ? parent.id : id
    end
  end
end

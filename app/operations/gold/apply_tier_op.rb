module Gold
  # Apply the tier that the shop has selected.
  class ApplyTierOp
    include Outcomes
    include Retries

    def initialize(billing)
      @billing = billing
    end

    def call
      begin
        tier = @billing.last_selected_tier
      rescue UnknownTier => e
        return TierNotFound.new(e.message)
      end

      @billing.transaction do
        unless %i[apply_tier apply_free_tier].include?(@billing.current_state)
          if tier.free?
            @billing.transition_to!(:apply_free_tier, tier_id: tier.id)
          elsif @billing.shopify_plan.paying?
            @billing.transition_to!(:apply_tier, tier_id: tier.id)
          end
        end

        @billing.tier = tier
        @billing.save!

        if tier.free?
          if Gold.configuration.allow_automated_charge_cancellation
            # Remove any outstanding active charges
            if (charge = ShopifyAPI::RecurringApplicationCharge.current)
              request_with_retries do
                Gold.logger.info("[#{@billing.id}] Free tier applied, canceling " \
                                "charge '#{charge.price}' for " \
                                "'#{@billing.shop.shopify_domain}'")
                charge.cancel
              end
            end
          else
            Gold.logger.info("[#{@billing.id}] Would have cancelled charge for " \
                            "'#{@billing.shop.shopify_domain}', but prevented by setting")
          end
        end

        @billing.transition_to!(billing_state)
      end

      Gold.configuration.on_apply_tier&.call(@billing, tier.id)
      TierApplied.new
    end

    private

    def billing_state
      if @billing.shopify_plan.affiliate?
        :affiliate
      elsif @billing.shopify_plan.staff?
        :staff
      else
        :billing
      end
    end
  end
end

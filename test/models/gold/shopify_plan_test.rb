require "test_helper"

module Gold
  class ShopifyPlanTest < ActiveSupport::TestCase
    def assert_all(plan, *queries)
      queries.each do |query|
        assert plan.send(query), "#{plan} should be #{query}"
      end
    end

    def refute_all(plan, *queries)
      queries.each do |query|
        refute plan.send(query), "#{plan} should be #{query}"
      end
    end

    def test_affiliate
      plan = ShopifyPlan.new("affiliate")
      assert_all plan, :affiliate?, :good?
      refute_all plan, :bad?, :cancelled?, :dormant?, :frozen?, :paying?, :staff?
    end

    def test_partner_test
      plan = ShopifyPlan.new("partner_test")
      assert :affiliate?
    end

    def test_staff
      plan = ShopifyPlan.new("staff")
      assert_all plan, :good?, :staff?
      refute_all plan, :affiliate?, :bad?, :cancelled?, :dormant?, :frozen?, :paying?

      assert ShopifyPlan.new("plus_partner_sandbox").staff?
    end

    %w[
      basic
      custom
      professional
      shopify_plus
      staff_business
      trial
      unlimited
    ].each do |paying|
      define_method("test_#{paying}") do
        plan = ShopifyPlan.new(paying)
        assert_all plan, :good?, :paying?
        refute_all plan, :affiliate?, :bad?, :cancelled?, :dormant?, :frozen?, :staff?
      end
    end

    def test_frozen
      plan = ShopifyPlan.new("frozen")
      assert_all plan, :bad?, :frozen?
      refute_all plan, :affiliate?, :cancelled?, :dormant?, :good?, :paying?, :staff?
    end

    def test_cancelled
      plan = ShopifyPlan.new("cancelled")
      assert_all plan, :bad?, :cancelled?
      refute_all plan, :affiliate?, :dormant?, :good?, :frozen?, :paying?, :staff?
    end

    def test_dormant
      plan = ShopifyPlan.new("dormant")
      assert_all plan, :dormant?, :good?, :paying?
      refute_all plan, :affiliate?, :bad?, :cancelled?, :frozen?, :staff?
    end
  end
end

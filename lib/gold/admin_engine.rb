module Gold
  # Brings the Gold to your app. Mount this in your config/routes.rb.
  class AdminEngine < ::Rails::Engine
    isolate_namespace Gold
    config.autoload_paths << File.expand_path("../../lib", __dir__)
    engine_name "gold_admin_engine"
  end
end

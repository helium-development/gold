## Changelog
Version 5.2.1
- Added `install` state between `new` and `accepted_terms`. It is now required to transition from `new` to `install` before transitioning to `accepted_terms`.

Version 5.2.0
- Child tiers now inherit features and qualifications from their parents.

Version 5.1.0
- Added `after_tier_redirect_path` configuration to allow apps to specify a redirect
  path after a merchant chooses a tier

Version 5.0.0
- Updated to Rails 6
- Updated to Shopify App 13

Version 4.2.0
- Added new route to start a merchant on a plan with a cookie
- Changed `config.referral_redirect_url` to `config.app_listing_url`

Version 4.0.0
- Added referral affiliates

Version 3.2.0
- Removed `force_embedded_redirect` config option
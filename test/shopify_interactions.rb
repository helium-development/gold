module Gold
  # This allows a test to more easily stub and verify interactions with Shopify.
  #
  # Example:
  #
  #   class FooTest < Minitest::Test
  #     include ShopifyInteractions
  #
  #     def test_foo
  #       stub_shopify_request(:post, "foos.json").to_return(status: 201)
  #
  #       foo = Foo.new(billing)
  #       with_shopify_session do
  #         assert_equal "Definitely Foo!", foo.call_shopify
  #       end
  #     end
  #   end
  module ShopifyInteractions
    # Returns a `Billing` instance for the shop under test.
    def billing
      @billing ||= Billing.create(shop: shop)
    end

    # Returns the shop under test.
    def shop
      @shop ||= ::Shop.create(
        shopify_domain: "example-#{rand(2**8)}.myshopify.com",
        shopify_token: SecureRandom.hex(16)
      )
    end

    # Returns the Shopify plan that the shop under test is using.
    def shopify_plan
      @shopify_plan ||= "basic"
    end

    # Sets the Shopify plan for the shop under test.
    def shopify_plan=(plan)
      @shopify_plan = plan
      @shop_properties[:plan_name] = plan if @shop_properties
    end

    # Returns values one might possibly get when requesting `/admin/shop.json`.
    # Note that this isn't a complete list, but rather a subset that Gold uses.
    def self.generate_shop_properties(domain, plan_name)
      owner_name = Faker::Name.name
      {
        country_code: Faker::Address.country_code,
        created_at: 1.month.ago,
        updated_at: 1.day.ago,
        email: Faker::Internet.email(owner_name),
        domain: Faker::Internet.domain_name,
        id: rand(2**32),
        myshopify_domain: domain,
        phone: Faker::PhoneNumber.phone_number,
        plan_name: plan_name,
        shop_owner: owner_name,
        has_storefront: true,
        setup_required: false
      }
    end

    # Returns the Shopify properties that would be returned when requesting
    # `/admin/shop.json` for the shop under test.
    def shop_properties
      @shop_properties ||= ShopifyInteractions.generate_shop_properties(
        shop.shopify_domain,
        shopify_plan
      )
    end

    # Override the shop under test's Shopify properties.
    def shop_properties=(properties)
      @shop_properties = properties
    end

    # Perform some Shopify API requests for the shop under test.
    def with_shopify_session(&block)
      stub!
      begin
        ShopifyAPI::Session.temp(
          domain: shop.shopify_domain,
          token: shop.shopify_token,
          api_version: Gold.configuration.shopify_api_version,
          &block
        )
      ensure
        unstub!
      end
    end

    # Convenience method to make stubbing requests to the Shopify shop under
    # test easier.
    def stub_shopify_request(method, path)
      version = Gold.configuration.shopify_api_version
      path = if path.is_a?(String)
               "/admin/api/#{version}/#{path.sub(%r{^/}, '')}"
             else
               path
             end

      url = if path.is_a? String
              URI::HTTPS.build(host: shop.shopify_domain, path: path).to_s
            else
              path
            end

      stub_request(method, url)
        .with(headers: { "X-Shopify-Access-Token" => shop.shopify_token })
    end

    # Supply a stub, and get back a parsed JSON body
    def json_requests_for_stub(stub)
      WebMock::RequestRegistry.instance.requested_signatures.each do |sig|
        yield JSON.parse(sig.body) if stub.matches?(sig)
      end
    end

    # Take a new store and transition it to a billing one.
    def set_up_shop_as_billing!(tier_id = :basic)
      with_shopify_session do
        transition_to!(:install)
        transition_to!(:accepted_terms)
        transition_to!(:select_tier, tier_id: tier_id)
        billing.update(trial_starts_at: Time.current)
        transition_to!(:sudden_charge, charge_id: "111111111")
        transition_to!(:sudden_charge_accepted)
        transition_to!(:charge_activated)
        transition_to!(:apply_tier, tier_id: tier_id)
        billing.update(tier_id: tier_id)
        transition_to!(:billing)
      end
    end

    # Set up a store as a Shopify Staff store.
    def set_up_shop_as_staff!(tier_id = :basic)
      self.shopify_plan = "staff"
      with_shopify_session do
        transition_to!(:install)
        transition_to!(:accepted_terms)
        transition_to!(:select_tier, tier_id: tier_id)
        billing.update(tier_id: tier_id)
        transition_to!(:staff)
      end
    end

    # Set up a store as an affiliate (development) store.
    def set_up_shop_as_affiliate!(tier_id = :basic)
      self.shopify_plan = "affiliate"
      with_shopify_session do
        transition_to!(:install)
        transition_to!(:accepted_terms)
        transition_to!(:select_tier, tier_id: tier_id)
        billing.update(tier_id: tier_id)
        transition_to!(:affiliate)
      end
    end

    # Set up a store on a free tier.
    def set_up_shop_as_free!
      with_shopify_session do
        transition_to!(:install)
        transition_to!(:accepted_terms)
        transition_to!(:select_tier, tier_id: :free)
        transition_to!(:apply_free_tier, tier_id: :free)
        billing.update(tier_id: :free)
        transition_to!(:billing)
      end
    end

    def transition_to!(*args)
      Gold::Coverage.exclude_from_coverage { billing.transition_to!(*args) }
    end

    # Clean up resources when we are done with them. Make sure to call `super`
    # if overriding this in a test class so that this is called properly.
    def teardown
      super
      @billing&.destroy
      @shop&.destroy
    end

    # Moves back time of transitions to mock events happening earlier
    def time_machine!(duration)
      billing.transitions.all.each do |t|
        seconds_back = 60 * 60 * 24 * duration
        t.update!(
          created_at: t.created_at - seconds_back,
          updated_at: t.updated_at - seconds_back
        )
      end
    end

    def with_tiers(yaml, &block)
      Tier.reset!
      Tier.stub :load_from_yaml, YAML.safe_load(yaml), &block
      Tier.reset!
    end

    private

    def stub!
      @stubs = [stub_request_to_get_admin_shop]
    end

    def unstub!
      @stubs.each { |s| remove_request_stub(s) }
      @stubs = []
    end

    # Stub requests to `/admin/shop.json` to return the shop properties that
    # have been defined for this test.
    def stub_request_to_get_admin_shop
      stub_shopify_request(:get, "shop.json")
        .to_return { { body: { shop: shop_properties }.to_json } }
    end
  end
end

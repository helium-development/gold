Rails.application.routes.draw do
  mount ShopifyApp::Engine, at: "/"
  mount Gold::Engine, at: "/payment", as: "gold_engine"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: "home#index"
end

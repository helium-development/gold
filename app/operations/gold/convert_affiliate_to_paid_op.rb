module Gold
  class ConversionError < StandardError; end

  # Convert an affiliate store to a paid store.
  class ConvertAffiliateToPaidOp
    def initialize(billing, return_url, test_charge = Gold.configuration.test_charge?)
      @billing = billing
      @return_url = return_url
      @test_charge = test_charge
    end

    def call
      unless @billing.shopify_plan.paying?
        raise ConversionError, "This is not a paying plan"
      end

      @billing.transition_to_or_stay_in!(:affiliate_to_paid)

      if @billing.last_selected_tier.free?
        ApplyTierOp.new(@billing).call
      else
        outcome = ChargeOp.new(@billing, @return_url, @test_charge).call

        if outcome.is_a? Outcomes::PendingCharge
          BillingMailer.affiliate_to_paid(@billing, outcome.confirmation_url).deliver_now
        end

        outcome
      end
    end
  end
end

require "test_helper"
require "shopify_interactions"
require "minitest/mock"

# rubocop:disable Metrics/LineLength
module Gold
  class AcceptOrDeclineChargeOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def setup
      @charge = {
        id: rand(2**32),
        name: Faker::App.name,
        price: Faker::Number.decimal(2),
        return_url: Faker::Internet.url,
        trial_days: Faker::Number.number(2).to_i,
        cancelled_on: nil,
        test: false
      }
    end

    def test_successful_sudden_charge
      set_up_shop_as_sudden_charge!
      accept_charge_and_verify
    end

    def test_successful_delayed_charge
      set_up_shop_as_delayed_charge!
      accept_charge_and_verify
    end

    def test_successful_optional_charge
      set_up_shop_as_optional_charge!
      accept_charge_and_verify
    end

    def test_activate_charge_retries
      set_up_shop_as_sudden_charge!

      path = "recurring_application_charges/#{@charge[:id]}.json"
      stub_shopify_request(:get, path)
        .to_return(status: 500).times(2).then
        .to_return(status: 200, body: {
          recurring_application_charge: @charge.merge(
            status: "accepted"
          )
        }.to_json)

      stub_shopify_request(:post, "recurring_application_charges/#{@charge[:id]}/activate.json")
        .to_return(status: 500).times(2).then
        .to_return(status: 200, body: {
          recurring_application_charge: @charge.merge(
            activated_on: Time.zone.today.strftime("%Y-%m-%d"),
            status: "active"
          )
        }.to_json)

      with_shopify_session do
        op = AcceptOrDeclineChargeOp.new(billing, @charge[:id])
        assert_instance_of Outcomes::ActiveCharge, op.call
      end
    end

    def test_activate_charge_server_error_fails
      set_up_shop_as_sudden_charge!
      stub_get_charge_with_status("accepted")

      stub_shopify_request(:get, "recurring_application_charges/#{@charge[:id]}.json")
        .to_return(status: 500)

      with_shopify_session do
        assert_raises ActiveResource::ServerError do
          AcceptOrDeclineChargeOp.new(billing, @charge[:id]).call
        end
      end
    end

    def test_previously_activated_charge_is_used
      set_up_shop_as_sudden_charge!
      stub_get_charge_with_status("active")

      with_shopify_session do
        op = AcceptOrDeclineChargeOp.new(billing, @charge[:id]).call
        assert op.ok?
      end

      assert_equal :charge_activated, billing.current_state
    end

    def test_declined_sudden_charge_fails
      set_up_shop_as_sudden_charge!
      decline_charge_and_verify :sudden_charge_declined
    end

    def test_declined_delayed_charge_fails
      set_up_shop_as_delayed_charge!
      decline_charge_and_verify :delayed_charge_declined
    end

    def test_declined_optional_charge_returns_to_billing
      set_up_shop_as_optional_charge!
      assert_equal :basic, billing.last_selected_tier.id
      decline_charge_and_verify :billing

      # Declining a charge should revert last selected tier
      assert_equal :free, Billing.find(billing.id).last_selected_tier.id
    end

    def test_expired_sudden_charge_fails
      set_up_shop_as_sudden_charge!
      expire_charge_and_verify :sudden_charge_expired
    end

    def test_expired_delayed_charge_fails
      set_up_shop_as_delayed_charge!
      expire_charge_and_verify :delayed_charge_expired
    end

    def test_expired_optional_charge_transitions
      set_up_shop_as_optional_charge!
      expire_charge_and_verify :optional_charge_declined
    end

    def test_forbidden_store_with_sudden_charge_fails
      set_up_shop_as_sudden_charge!
      uninstall_and_verify
    end

    def test_forbidden_store_with_delayed_charge_fails
      set_up_shop_as_delayed_charge!
      uninstall_and_verify
    end

    def test_forbidden_store_with_optional_charge_fails
      set_up_shop_as_optional_charge!
      uninstall_and_verify
    end

    def test_cancelled_charge_fails
      set_up_shop_as_sudden_charge!
      stub_get_charge_with_status("cancelled")

      with_shopify_session do
        op = AcceptOrDeclineChargeOp.new(billing, @charge[:id]).call
        refute op.ok?
        assert_instance_of Outcomes::CannotProcessCharge, op
      end
    end

    def test_pending_charge_fails
      set_up_shop_as_sudden_charge!
      stub_get_charge_with_status("pending")

      with_shopify_session do
        op = AcceptOrDeclineChargeOp.new(billing, @charge[:id]).call
        refute op.ok?
        assert_instance_of Outcomes::CannotProcessCharge, op
      end
    end

    def test_activate_charge_mismatch_fails
      set_up_shop_as_sudden_charge!
      stub_get_charge_with_status("accepted")

      charge_id = @charge[:id] - 1

      with_shopify_session do
        op = AcceptOrDeclineChargeOp.new(billing, charge_id).call
        refute op.ok?
        assert_instance_of Outcomes::CannotProcessCharge, op
      end
    end

    private

    # Take a new store and give it a sudden charge.
    def set_up_shop_as_sudden_charge!
      with_shopify_session do
        transition_to!(:install)
        transition_to!(:accepted_terms)
        transition_to!(:select_tier, tier_id: :basic)
        transition_to!(:sudden_charge, charge_id: @charge[:id])
      end
    end

    # Take a new store and give it a delayed charge.
    def set_up_shop_as_delayed_charge!
      with_shopify_session do
        transition_to!(:install)
        transition_to!(:accepted_terms)
        transition_to!(:select_tier, tier_id: :free)
        transition_to!(:apply_free_tier, tier_id: :free)
        transition_to!(:billing)
        transition_to!(:calculate_usage)
        transition_to!(:auto_upgrade_tier, tier_id: :basic)
        transition_to!(:delayed_charge, charge_id: @charge[:id])
      end
    end

    # Take a new store and give it an optional charge.
    def set_up_shop_as_optional_charge!
      with_shopify_session do
        transition_to!(:install)
        transition_to!(:accepted_terms)
        transition_to!(:select_tier, tier_id: :free)
        transition_to!(:apply_free_tier, tier_id: :free)
        billing.update(tier_id: "free")
        transition_to!(:billing)
        transition_to!(:change_tier, tier_id: :basic)
        transition_to!(:optional_charge, charge_id: @charge[:id])
      end
    end

    def stub_get_charge(charge)
      stub_shopify_request(:get, "recurring_application_charges/#{@charge[:id]}.json")
        .to_return(status: 200, body: {
          recurring_application_charge: charge
        }.to_json)
    end

    def stub_get_charge_with_status(status)
      stub_get_charge(@charge.merge(status: status))
    end

    def activate_path(charge_id)
      "recurring_application_charges/#{charge_id}/activate.json"
    end

    def accept_charge_and_verify
      stub_get_charge_with_status("accepted")

      stub = stub_shopify_request(:post, activate_path(@charge[:id]))
      stub.to_return(status: 200, body: {
        recurring_application_charge: @charge.merge(
          activated_on: Time.zone.today.strftime("%Y-%m-%d"),
          status: "active"
        )
      }.to_json)

      mock = MiniTest::Mock.new
      mock.expect(:call, nil, [billing, ShopifyAPI::RecurringApplicationCharge])
      Gold.configuration.on_activate_charge = mock

      outcome = with_shopify_session do
        AcceptOrDeclineChargeOp.new(billing, @charge[:id]).call
      end

      assert outcome.ok?
      assert_instance_of Outcomes::ActiveCharge, outcome
      assert_requested stub
      assert_mock mock
      assert_equal :charge_activated, billing.current_state
    ensure
      Gold.configuration.on_activate_charge = nil
    end

    def decline_charge_and_verify(expected_state)
      stub_get_charge_with_status("declined")

      outcome = with_shopify_session do
        AcceptOrDeclineChargeOp.new(billing, @charge[:id]).call
      end

      refute outcome.ok?
      assert_instance_of Outcomes::DeclinedCharge, outcome
      assert_equal expected_state, billing.current_state
    end

    def expire_charge_and_verify(expected_state)
      stub_get_charge_with_status("expired")

      outcome = with_shopify_session do
        AcceptOrDeclineChargeOp.new(billing, @charge[:id]).call
      end

      refute outcome.ok?
      assert_instance_of Outcomes::ExpiredCharge, outcome
      assert_equal expected_state, billing.current_state
    end

    def uninstall_and_verify
      stub_shopify_request(:get, %r{myshopify.com/admin/})
        .to_return(status: 403)

      outcome = with_shopify_session do
        AcceptOrDeclineChargeOp.new(billing, @charge[:id]).call
      end

      refute outcome.ok?
      assert_instance_of Outcomes::Uninstalled, outcome
      assert_equal :marked_as_uninstalled, billing.current_state
    end
  end
end
# rubocop:enable Metrics/LineLength

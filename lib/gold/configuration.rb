module Gold
  # Config for Gold
  class Configuration
    attr_accessor :test_charge,
                  :app_name,
                  :contact_email,
                  :plan_comparison_url,
                  :logger,
                  :admin_credentials,
                  :shopify_api_version,
                  :allow_automated_charge_cancellation,
                  :days_until_delinquent,
                  :days_until_cleanup,
                  :shop_domain_attribute,
                  :force_embedded_redirect,
                  :app_listing_url,
                  :after_tier_redirect_path

    # Callbacks
    attr_accessor :on_terms,
                  :on_install,
                  :on_activate_charge,
                  :on_apply_tier,
                  :on_uninstall,
                  :on_cleanup,
                  :on_check_charge

    attr_writer :shop_class

    def initialize
      # The class name (string) of the the app's Active Record Shop model
      @shop_class = nil

      # The attribute of the *.myshopify.com domain on the Shop model
      @shop_domain_attribute = nil

      # Should Gold create test charges? Helpful in a development environment
      @test_charge = false

      # The logger Gold will use
      @logger = Logger.new(STDOUT)

      # How many days we'll wait before cleaning up an uninstalled shop
      @days_until_cleanup = 30

      # How many days we'll wait before we uninstall a shop without payment
      @days_until_delinquent = 7

      # The name of the app using gold (used in the charge name)
      @app_name = "My App"

      # The email Gold uses to send emails from
      @contact_email = nil

      # The URL to a plan comparison page for the app
      @plan_comparison_url = nil

      # Force Gold billing view's to redirect to iframe
      @force_embedded_redirect = false

      # Login credential's to Gold's backend
      @admin_credentials = {
        user: "admin",
        password: "password123"
      }

      # The API version used by Shopify (https://help.shopify.com/en/api/versioning)
      @shopify_api_version = "2019-04"

      # If Gold is allowed to cancel charges (paid -> free) automatically
      @allow_automated_charge_cancellation = true

      # The URL to follow after a referral code is tracked
      # (e.g. https://apps.shopify.com/customr)
      @app_listing_url = "https://apps.shopify.com/"

      # The redirect path after a tier is applied. Use this path to welcome a
      # merchant if they just installed
      @after_tier_redirect_path = "/"
    end

    def shop_class
      @shop_constant ||= @shop_class.to_s.constantize
    rescue NameError
      raise "'#{@shop_class}' is not a valid class name for shop class. " \
            "You must specify one in your Gold config file with " \
            "'config.shop'"
    end

    def test_charge?
      @test_charge
    end
  end
end

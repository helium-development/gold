module Gold
  # Stores transitions between states in Gold::Machine.
  class Transition < ApplicationRecord
    # If your transition table doesn't have the default `updated_at` timestamp column,
    # you'll need to configure the `updated_timestamp_column` option, setting it to
    # another column name (e.g. `:updated_on`) or `nil`.
    #
    # self.updated_timestamp_column = :updated_on
    # self.updated_timestamp_column = nil

    belongs_to :billing, inverse_of: :transitions

    after_destroy :update_most_recent, if: :most_recent?

    private

    def update_most_recent
      last_transition = billing.transitions.order(:sort_key).last
      return if last_transition.blank?

      # rubocop:disable Rails/SkipsModelValidations
      last_transition.update_column(:most_recent, true)
      # rubocop:enable Rails/SkipsModelValidations
    end
  end
end

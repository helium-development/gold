class Subscription < ActiveRecord::Base
  belongs_to :shop
  before_create :cancel_previous

  scope :active, -> { where(cancelled_at: nil) }

  def first_payment
    activated_on + trial_days.days
  end

  def trial_days_left
    days = (first_payment - today).to_i

    # Don't return a negative number
    days > 0 ? days : 0
  end

  def trial_ended?
    !(trial_days_left > 0)
  end

  # How many days we've paid through
  def pro_rated_days
    if trial_ended?
      days_since_first_payment = (today - first_payment).to_i
      30 - (days_since_first_payment % 30)
    else
      0
    end
  end

  def cancelled?
    !cancelled_at.nil?
  end

  def status
    if cancelled?
      "cancelled"
    else
      "active"
    end
  end

  def price_in_dollars
    price / 100.0
  end

  private

  def today
    Time.now.utc.to_date
  end

  # Cancel any previous subscriptions
  def cancel_previous
    shop.cancel_subscriptions!
  end
end

module Gold
  # Run when the app is installed, reinstalled, or a user logs in. This is
  # essentially how Gold is bootstrapped, so there is an assumption made
  # here (the shop class has a `#with_shopify_session` method). This is true
  # with the default Shop class that Shopify sets up, but if that is different
  # for your environment, you will need to create your own custom version of
  # this job.
  class AfterAuthenticateJob < Gold::ApplicationJob
    include Outcomes

    def perform(shop_domain:)
      # Look up the shop associated with the Shopify session.
      shop = Gold.shop_class.find_by!(Gold.shop_domain_attribute => shop_domain)

      # Create a billing instance if this store is newly installed or already
      # cleaned up.
      billing = Billing.find_or_create_by!(shop: shop)

      shop.with_shopify_session do
        outcome = InstallOp.new(billing).call

        case outcome
        when ChargeNeeded, MissingCharge
          process_charge_url = Engine.routes.url_helpers.process_charge_url
          logger.info("#{shop_domain} has reinstalled and needs to approve a " \
                      "charge for their selected tier")
          ChargeOp.new(billing, process_charge_url).call
        when Success
          logger.info("#{shop_domain} successfully installed the app")
        end

        outcome
      end
    end
  end
end

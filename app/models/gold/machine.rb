require "statesman"

module Gold
  # This is the finite state machine specification that governs how shops may
  # transition from one state to another.
  class Machine
    include Statesman::Machine

    # rubocop:disable Metrics/LineLength

    # States

    state :new, initial: true

    # A merchant has installed the app
    state :install

    # Before merchants can use our app, they will need to review and accept our
    # Terms of Service.
    state :accepted_terms

    # Merchants will need to select a plan/tier that meets their needs upon
    # installing. They can also change their tier later. The new desired tier
    # won't be applied until in :apply_tier.
    state :select_tier
    state :apply_tier
    state :apply_free_tier

    # When a paid store initially installs the app, they will have to accept
    # billing charges before being able to use the app.
    state :charge_activated
    state :sudden_charge
    state :sudden_charge_accepted
    state :sudden_charge_expired
    state :sudden_charge_declined

    # Billing is where most stores will be. All of the necessary charges have
    # been set up, the store is in good standing, etc.
    state :billing

    # We might choose to give merchants a one-time credit towards their bill
    # for our app.
    state :issue_credit

    # We might choose to give merchants a discount on our app.
    state :apply_discount

    # A merchant might want to change the tier that they are on.
    state :change_tier

    # On a daily basis, we want to sync with Shopify to make sure a store's
    # charge exists on their end. If we missed a webhook, they could have
    # cancelled the charge somehow, but still be getting service from us.
    state :check_charge
    state :charge_missing

    # Periodically, we will evaluate a store's usage of our app to see if they
    # belong in a different tier. We may move them between tiers automatically.
    state :calculate_usage
    state :auto_upgrade_tier

    # If a merchant changes tiers manually, they will be presented with new
    # billing charges. If they accept these, the new tier will be applied. If
    # they don't accept the new charges, they will stay on their existing tier.
    state :optional_charge
    state :optional_charge_declined
    state :optional_charge_accepted

    # If we automatically shift a merchant to a new tier, give them a sustained
    # discount, they move from an affiliate to paid Shopify plan, etc, we will
    # need to adjust their billing charges. This is similar to when they install
    # the app at first, but we include a grace period to let them accept the new
    # charges without immediately shutting off their use of our app.
    state :delayed_charge
    state :delayed_charge_accepted
    state :delayed_charge_expired
    state :delayed_charge_declined

    state :marked_as_delinquent
    state :delinquent

    # Affiliate (development) stores are free stores that are not yet
    # accessible to the world. They are created by Shopify partners and handed
    # off to merchants when they go live. They will become paying stores with
    # that transition.
    state :affiliate
    state :affiliate_to_paid

    # Shopify staff may want to try our app. We provide it to them for free.
    state :staff

    # If a store fails to comply with our terms of service, we can mark them as
    # suspended.
    state :marked_as_suspended
    state :suspended

    # Shopify may freeze a store if they fail to pay in a timely manner. We will
    # learn about this through webhooks and suspend their service accordingly.
    state :frozen

    # Users may choose to uninstall our app or not respond to us in a timely
    # manner, in which case their use of the app will be revoked and eventually
    # cleaned up.
    state :marked_as_uninstalled
    state :uninstalled
    state :reinstalled
    state :cleanup
    state :done

    # These are stable states that a store can be in for an indefinite amount of
    # time. Any watchdog task should ignore these states.
    ACCEPTANCE_STATES = %i[affiliate billing done staff].freeze

    # Whether this state machine is currently in an acceptance state.
    def accepted?
      ACCEPTANCE_STATES.include?(current_state.to_sym)
    end

    def current_state
      super.to_sym
    end

    # Guards

    # Ensure that a shop has a certain type of plan before allowing a transition.
    def self.ensure_plan_is(query_method)
      test = proc { |billing| billing.shopify_plan.send(query_method) }
      test.define_singleton_method(:to_s) { "Billing is #{query_method}?" }
      test
    end

    def self.ensure_tier_is_not_free
      test = proc { |billing| !billing.tier&.free? }
      test.define_singleton_method(:to_s) { "Tier is not free?" }
      test
    end

    def self.ensure_tier_is_free
      test = proc { |billing| billing.tier&.free? }
      test.define_singleton_method(:to_s) { "Tier is free?" }
      test
    end

    # Ensure that a shop has been in a current state for longer than number of days
    def self.ensure_min_days_in_state(duration_in_days)
      test = proc { |billing|
        billing.state_machine.last_transition.updated_at < duration_in_days.days.ago
      }
      test.define_singleton_method(:to_s) { "After #{duration_in_days} days?" }
      test
    end

    # Ensure that certain metadata properties are set before allowing a
    # transition.
    def self.require_metadata(*properties)
      proc do |_, transition|
        properties.each do |property|
          # rubocop:disable Style/Next
          unless transition.metadata.key?(property.to_sym) ||
                 transition.metadata.key?(property.to_s)
            message = "Transition to #{transition.to_state} needs to have #{property} set"
            raise Gold::Exceptions::MetadataMissing, message
          end
          # rubocop:enable Style/Next
        end
      end
    end

    # Transitions

    transition from: :new, to: %i[install marked_as_uninstalled]
    transition from: :install, to: %i[accepted_terms marked_as_uninstalled]
    transition from: :accepted_terms, to: %i[select_tier marked_as_uninstalled]

    before_transition to: :select_tier, &require_metadata(:tier_id)
    transition from: :select_tier, to: %i[affiliate
                                          apply_free_tier
                                          sudden_charge
                                          select_tier
                                          marked_as_uninstalled
                                          staff]

    before_transition to: :sudden_charge, &require_metadata(:charge_id)
    guard_transition to: :sudden_charge, &ensure_plan_is(:paying?)
    transition from: :sudden_charge, to: %i[sudden_charge_accepted
                                            sudden_charge_declined
                                            sudden_charge_expired
                                            select_tier
                                            marked_as_uninstalled]
    transition from: :sudden_charge_accepted, to: %i[charge_activated
                                                     marked_as_uninstalled]
    transition from: :sudden_charge_expired, to: %i[sudden_charge
                                                    marked_as_uninstalled]
    transition from: :sudden_charge_declined, to: %i[sudden_charge select_tier
                                                     marked_as_uninstalled]

    transition from: :charge_activated, to: :apply_tier
    before_transition to: :apply_tier, &require_metadata(:tier_id)
    transition from: :apply_tier, to: :billing
    before_transition to: :apply_free_tier, &require_metadata(:tier_id)
    transition from: :apply_free_tier, to: %i[billing staff affiliate]

    transition from: :billing, to: %i[apply_discount
                                      calculate_usage
                                      change_tier
                                      check_charge
                                      frozen
                                      issue_credit
                                      marked_as_suspended
                                      marked_as_uninstalled]
    guard_transition to: :check_charge, &ensure_tier_is_not_free
    transition from: :check_charge, to: %i[check_charge
                                           billing
                                           charge_missing
                                           frozen
                                           marked_as_uninstalled]
    guard_transition from: :check_charge,
                     to: :billing,
                     &ensure_plan_is(:paying?)

    transition from: :charge_missing, to: %i[delayed_charge marked_as_uninstalled]

    transition from: :calculate_usage, to: %i[auto_upgrade_tier billing]

    before_transition to: :auto_upgrade_tier, &require_metadata(:tier_id)
    transition from: :auto_upgrade_tier, to: :delayed_charge
    transition from: :apply_discount, to: %i[optional_charge billing apply_discount]
    guard_transition to: :apply_discount, &ensure_tier_is_not_free

    transition from: :issue_credit, to: %i[billing marked_as_uninstalled]

    before_transition to: :change_tier, &require_metadata(:tier_id)
    transition from: :change_tier, to: %i[affiliate
                                          apply_free_tier
                                          change_tier
                                          staff
                                          optional_charge
                                          marked_as_uninstalled]

    before_transition to: :optional_charge, &require_metadata(:charge_id)
    guard_transition to: :optional_charge, &ensure_plan_is(:paying?)
    transition from: :optional_charge, to: %i[change_tier
                                              optional_charge_accepted
                                              optional_charge_declined
                                              marked_as_uninstalled]
    transition from: :optional_charge_declined, to: %i[billing
                                                       marked_as_uninstalled]

    transition from: :optional_charge_accepted, to: %i[charge_activated
                                                       marked_as_uninstalled]

    before_transition to: :delayed_charge, &require_metadata(:charge_id)
    guard_transition to: :delayed_charge, &ensure_plan_is(:paying?)
    transition from: :delayed_charge, to: %i[marked_as_delinquent
                                             select_tier
                                             delayed_charge_accepted
                                             delayed_charge_declined
                                             delayed_charge_expired
                                             marked_as_uninstalled]

    transition from: :delayed_charge_accepted, to: %i[charge_activated
                                                      marked_as_uninstalled]

    transition from: :delayed_charge_expired, to: %i[delayed_charge
                                                     marked_as_delinquent
                                                     marked_as_uninstalled]
    transition from: :delayed_charge_declined, to: %i[marked_as_delinquent
                                                      delayed_charge
                                                      marked_as_uninstalled]

    guard_transition to: :marked_as_delinquent,
                     &ensure_min_days_in_state(Gold.configuration.days_until_delinquent)
    transition from: :marked_as_delinquent, to: %i[delinquent
                                                   marked_as_uninstalled]

    transition from: :delinquent, to: %i[marked_as_uninstalled delayed_charge cleanup]

    guard_transition to: :affiliate, &ensure_plan_is(:affiliate?)
    transition from: :affiliate, to: %i[affiliate_to_paid
                                        change_tier
                                        marked_as_suspended
                                        marked_as_uninstalled]
    transition from: :affiliate_to_paid, to: %i[apply_free_tier
                                                delayed_charge]

    guard_transition to: :staff, &ensure_plan_is(:staff?)
    transition from: :staff, to: %i[change_tier marked_as_uninstalled]

    guard_transition to: :frozen, &ensure_plan_is(:frozen?)
    transition from: :frozen, to: %i[check_charge
                                     cleanup
                                     marked_as_uninstalled]

    transition from: :marked_as_suspended, to: :suspended
    transition from: :suspended, to: %i[affiliate
                                        check_charge
                                        billing
                                        cleanup]
    guard_transition from: :suspended, to: :billing, &ensure_tier_is_free

    transition from: :marked_as_uninstalled, to: %i[uninstalled]
    transition from: :uninstalled, to: %i[cleanup reinstalled]
    transition from: :reinstalled, to: %i[select_tier
                                          marked_as_uninstalled]

    guard_transition to: :cleanup, &ensure_min_days_in_state(Gold.configuration.days_until_cleanup)
    transition from: :cleanup, to: :done
    transition from: :done, to: :reinstalled

    # rubocop:enable Metrics/LineLength

    # Helpers

    # Like `last_transition` but returns the last transition to one of the states
    # provided as arguments. Returns nil if no transitions match.
    def last_transition_to(*states)
      history.reverse.find { |t| states.include?(t.to_state.to_sym) }
    end

    # Transition only if different from the current state
    def transition_to_or_stay_in!(state, metadata = nil)
      transition_to!(state, metadata) unless current_state == state
    end

    # Soft transition only if different from the current state
    def transition_to_or_stay_in(state, metadata = nil)
      current_state == state ? true : transition_to(state, metadata)
    end
  end
end

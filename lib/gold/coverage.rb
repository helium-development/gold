module Gold
  # Captures coverage on Statesman state machines. Include this module into the
  # `Statesman::Machine` subclass that you want to record usage for. Results may
  # be accessed with `#coverage` on that class (not the instances).
  module Coverage
    def self.included(klass)
      klass.extend(ClassMethods)
    end

    # Class methods to add to the state machine when Gold::Coverage is included.
    module ClassMethods
      def coverage
        @coverage ||= Results.new(self)
      end
    end

    # Whether or not we are collecting coverage information currently.
    def self.covered?
      @covered = @covered.nil? || @covered
    end

    # Set whether we are collecting coverage information currently.
    def self.covered=(covered)
      @covered = covered
    end

    # Execute a block and don't include it in the coverage results.
    def self.exclude_from_coverage
      original = covered?
      self.covered = false
      result = yield
      self.covered = original
      result
    end

    # Describes a transition between two states.
    Transition = Struct.new(:from, :to)

    # Holds the collected coverage data.
    class Results
      def initialize(machine)
        @machine = machine
        @covered = Set.new
      end

      # Records when a transition is made from one state to another.
      def cover(from, to)
        @covered.add(Transition.new(from, to))
      end

      # Iterates over transitions that have been made thus far. This is in the
      # same form as `Statesman::Machine#successors`, so the two can be compared
      # to see where coverage is lacking.
      def to_h
        @covered.each_with_object(Hash.new { [] }) do |transition, hash|
          hash[transition.from] = hash[transition.from] << transition.to
          hash
        end
      end

      # Generates an SVG diagram of the coverage that has been captured so far.
      # Covered transitions are colored green and uncovered transitions are
      # colored red.
      def to_svg(path)
        coverage = to_h
        callback = lambda do |edge|
          from = edge.tail_node
          to = edge.head_node
          edge[:color] =
            coverage.key?(from) && coverage[from].include?(to) ? "green" : "red"
        end
        Gold::Diagram.new(@machine, edge_callback: callback).render(path)
      end
    end

    # Override `#transition_to!` to observe the current state and the new state.
    # Statesman's callbacks do not currently allow for this, hence the need to
    # intercept here. `#transition_to` calls `#transition_to!`, so it is
    # sufficient to tap into `Statesman::Machine` at this one point.
    def transition_to!(new_state, metadata = {})
      initial_state = current_state
      super(new_state, metadata)
      if Gold::Coverage.covered?
        self.class.coverage.cover(initial_state.to_s, new_state.to_s)
      end
      true
    end
  end
end

require_relative "gold/version"

require "shopify_app"

require_relative "gold/billing"
require_relative "gold/shop"
require_relative "gold/subscription"
require_relative "gold/session"
require_relative "gold/engine"
require_relative "gold/helpers"

ActionView::Base.send :include, Gold::Helpers

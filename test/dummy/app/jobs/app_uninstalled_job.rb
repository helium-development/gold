class AppUninstalledJob < ApplicationJob
  # Gold overrides this. Use `Gold.configuration.on_uninstall` to hook into this process
  def perform(shop_domain)
    Gold::AppUninstalledJob.new.perform(shop_domain)
  end
end

module Gold
  module Session
    # Checks to see if the shop needs to pay, will redirect them to accept
    def validate_plan
      if current_shop.payment_required?
        charge = current_shop.active_charge

        # Check if this store has an active charge. If they do, then just
        # subscribe them to that. Otherwise, create a new charge
        if charge
          if charge.price.to_f == current_shop.monthly_price_in_dollars
            current_shop.subscribe(current_shop.active_charge)
          else
            setup_billing
          end
        else
          setup_billing
        end
      end
    end

    def setup_billing
      fullpage_redirect_to(gold.billing_setup_path)
    end
  end
end

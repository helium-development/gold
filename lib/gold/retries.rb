module Gold
  # For handling network request retries
  module Retries
    def request_with_retries(max_attempts = 3)
      attempts = 0

      begin
        attempts += 1
        yield
      rescue ActiveResource::ConnectionError => e
        # Maybe this is a transient error? Try it again up to the max amount
        Gold.logger.error("Failed to make request: #{e}")

        case e
        when ActiveResource::ServerError
          Gold.logger.info e.response.code.inspect
          raise e unless %w[500 502 503 504].include?(e.response.code)
        when ActiveResource::SSLError
          Gold.logger.warn "SSL Error"
        when ActiveResource::TimeoutError
          Gold.logger.warn "Timeout Error"
        else
          raise e
        end

        retry unless attempts > max_attempts
        raise e
      end
    end
  end
end

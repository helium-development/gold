# This class demonstrates that Gold can be mixed into classes other than a
# "Shop" class.
class Merchant < ApplicationRecord
  include Gold::Concerns::Gilded
end

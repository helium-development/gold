module Gold
  class SetupController < ApplicationController
    def new
      tier = Tier.find(params[:tier])
      cookies[:gold_tier_id] = { value: tier.id, expires: 7.days } if tier
      redirect_to Gold.configuration.app_listing_url
    end
  end
end
require "bigdecimal"

module Gold
  # Migrates previous systems to Gold for a specific shop
  class BillingMigrator
    attr_reader :shop,
                :tier_id,
                :tier,
                :billing

    def initialize(shop, tier_id, trial_starts_at = nil)
      @shop = shop
      @tier_id = tier_id.to_sym
      @trial_starts_at = trial_starts_at
    end

    # Finds an appropriate tier based on tier_id and current charge
    def lookup_tier!
      charge = ShopifyAPI::RecurringApplicationCharge.current

      # First attempt to find a matching tier with name and price
      tier = Tier.all.find do |t|
        (t.id == @tier_id || t.parent&.id == @tier_id) &&
          tier_matches_charge_price(t, charge)
      end

      # Fallback on just tier id
      tier ||= Tier.find(@tier_id)

      # Raise an exception if no tier is found
      raise Outcomes::TierNotFound unless tier

      @tier = tier
    end

    def migrate!
      if Billing.find_by(shop_id: shop.id)
        Gold.logger.warn("Attempted to migrate shop '#{@shop.shopify_domain}'" \
                         ", but Billing record already exists")
        return false
      end

      @billing = Billing.create!(
        shop: @shop,
        trial_starts_at: @trial_starts_at
      )

      @billing.transition_to! :install

      lookup_tier!
      accept_terms
      select_tier

      Gold.logger.info("Migrated shop '#{@shop.shopify_domain}', state is " \
                       "'#{@billing.current_state}', tier is '#{@tier.id}'")

      true
    rescue ActiveResource::UnauthorizedAccess,
           ActiveResource::ForbiddenAccess,
           ActiveResource::ResourceNotFound
      # Shop is uninstalled, that's OK
      Gold.logger.info("Shop '#{@shop.shopify_domain}' is uninstalled")
      true
    end

    private

    def accept_terms
      AcceptOrDeclineTermsOp.new(@billing, true).call
    end

    def select_tier
      outcome = SelectTierOp.new(@billing, @tier, false).call

      charge_op if outcome.is_a?(Outcomes::ChargeNeeded)
    end

    def charge_op
      return_url = Engine.routes.url_helpers.process_charge_url
      ChargeOp.new(@billing, return_url).call

      charge_transition = @billing.state_machine.last_transition_to(:sudden_charge)

      unless charge_transition
        raise "Expected billing '#{@billing.id}' to have sudden_charge transition"
      end

      accept_charge(charge_transition.metadata["charge_id"])
    end

    def accept_charge(charge_id)
      outcome = AcceptOrDeclineChargeOp.new(@billing, charge_id).call

      if outcome.ok?
        apply_tier
      else
        Rails.logger.info("Charge for '#{@shop.shopify_domain}' was '#{outcome}'")
      end
    end

    def apply_tier
      ApplyTierOp.new(@billing).call.ok?
    end

    def tier_matches_charge_price(tier, charge)
      return false unless charge

      tier.monthly_price == charge.price.to_d
    end
  end
end

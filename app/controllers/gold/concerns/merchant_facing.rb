module Gold
  module Concerns
    # Helpers that make it easier to work with the current merchant's billing
    # information in controllers.
    module MerchantFacing
      def self.included(base)
        base.helper_method :billing
      end

      # Returns the Gold::Billing instance for the currently logged-in merchant.
      def billing
        @billing ||= Billing.find_by!(shop_id: session[:shop_id])
      rescue ActiveRecord::RecordNotFound
        raise Gold::Exceptions::BillingNotFound
      end

      # `before_action` filter that forces a merchant to deal with
      # billing-related states (outstanding charges, acceptance of terms, etc)
      # before continuing with their work.
      def confront_mandatory_billing_action
        ConfrontMandatoryBillingAction.new(self).call
      end

      # Provides access to routes, passing in a particular controller as
      # context. We go through the effort of putting this into a separate class
      # because we do not want to directly include the url helpers into whatever
      # controller we are included into because that might override routes. This
      # solution isolates the Gold routes from whatever routes the app that Gold
      # is embedded in may have defined.
      class HasAccessToRoutes
        include Engine.routes.url_helpers
        delegate :default_url_options, :url_options, to: :@context

        def initialize(context)
          @context = context
          @engine = context.gold_engine
        end
      end

      # See documentation on `confront_mandatory_billing_action`.
      class ConfrontMandatoryBillingAction < HasAccessToRoutes
        def call
          begin
            state = @context.billing.current_state
          rescue ActiveRecord::RecordNotFound
            state = nil
          end

          Gold.logger.info("Confronting billing, state is '#{state}'")

          case state
          when nil, :new, :install
            Gold.logger.info("Redirecting to terms page...")
            return @context.redirect_to(@engine.terms_url)
          when :select_tier, :reinstalled, :accepted_terms
            Gold.logger.info("Redirecting to select tier...")
            return @context.redirect_to(@engine.select_tier_url)
          when :charge_missing
            Gold.logger.info("Redirecting to missing charge...")
            return @context.redirect_to(@engine.missing_charge_url)
          when :sudden_charge, :delayed_charge, :optional_charge
            Gold.logger.info("Redirecting to outstanding charge...")
            return @context.redirect_to(@engine.outstanding_charge_url)
          when :sudden_charge_declined, :delayed_charge_declined
            Gold.logger.info("Redirecting to declined charge...")
            return @context.redirect_to(@engine.declined_charge_url)
          when :sudden_charge_expired, :delayed_charge_expired
            Gold.logger.info("Redirecting to expired charge...")
            return @context.redirect_to(@engine.expired_charge_url)
          when :marked_as_delinquent, :delinquent
            Gold.logger.info("Redirecting to missing charge...")
            return @context.redirect_to(@engine.missing_charge_url)
          when :marked_as_suspended, :suspended
            Gold.logger.info("Redirecting to suspended page...")
            return @context.redirect_to(@engine.suspended_path)
          when :marked_as_uninstalled, :uninstalled, :frozen, :done
            Gold.logger.info("Redirecting to uninstalled page...")
            return @context.redirect_to(@engine.uninstalled_path)
          when :cleanup
            Gold.logger.info("Shop is cleanup, redirecting to unavailable...")
            return @context.redirect_to(@engine.unavailable_path)
          end
        end
      end
    end
  end
end

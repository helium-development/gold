require "test_helper"
require "shopify_interactions"
require "minitest/mock"

module Gold
  class CheckChargeOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def test_has_active_charge_when_billing
      tier = Tier.find(:advanced)
      set_up_shop_as_billing!(tier.id)
      verify_has_active_charge(billing)
    end

    def test_has_active_charge_when_unfrozen
      tier = Tier.find(:advanced)
      set_up_shop_as_billing!(tier.id)
      billing.shopify_plan_override = "frozen"
      transition_to!(:frozen)
      billing.shopify_plan_override = nil
      verify_has_active_charge(billing)
    end

    def test_missing_active_charge
      tier = Tier.find(:advanced)
      set_up_shop_as_billing!(tier.id)

      stub_get_charges([
        {
          name: "Old Plan",
          price: "15.22",
          status: "cancelled"
        },
        {
          name: "Legacy",
          price: "19.99",
          status: "pending"
        }
      ])

      with_shopify_session do
        op = CheckChargeOp.new(billing).call
        refute op.ok?
        assert_instance_of Outcomes::MissingCharge, op
      end

      assert_equal :charge_missing, billing.current_state
    end

    def test_get_charges_retries
      tier = Tier.find(:advanced)
      set_up_shop_as_billing!(tier.id)

      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 500).times(3).then
        .to_return(status: 200, body: {
          recurring_application_charges: [
            {
              name: tier.name,
              price: tier.monthly_price.to_s,
              trial_days: tier.trial_days,
              test: false,
              status: "active"
            }
          ]
        }.to_json)

      with_shopify_session do
        op = CheckChargeOp.new(billing).call
        assert op.ok?
      end
    end

    def test_mismatch_charge_fails
      tier = Tier.find(:pro)
      set_up_shop_as_billing!(tier.id)

      stub_get_charges([
        {
          name: "Plaster",
          price: "4.50",
          status: "active",
          trial_days: tier.trial_days,
          test: false
        }
      ])

      with_shopify_session do
        op = CheckChargeOp.new(billing).call
        refute op.ok?
        assert_instance_of Outcomes::MismatchCharge, op
      end

      assert_equal :charge_missing, billing.current_state
    end

    # Ensure we don't get stuck if something fails into 'check_charge' state
    def test_can_retry
      set_up_shop_as_billing!(:basic)
      billing.transition_to!(:check_charge)

      stub_get_charges([])

      with_shopify_session do
        op = CheckChargeOp.new(billing).call
        assert_instance_of Outcomes::MissingCharge, op
      end

      assert_equal :charge_missing, billing.current_state
    end

    def test_no_active_charges
      set_up_shop_as_billing!(:basic)

      stub_get_charges([
        {
          name: "Conservation of MBP",
          price: "11.20",
          status: "pending"
        }
      ])

      with_shopify_session do
        op = CheckChargeOp.new(billing).call
        assert_instance_of Outcomes::MissingCharge, op
      end

      assert_equal :charge_missing, billing.current_state
    end

    def test_executes_callback
      set_up_shop_as_billing!(:basic)

      stub_get_charges([
        {
          name: "Basic",
          price: billing.tier.monthly_price,
          status: "active",
          test: false
        }
      ])

      mock = MiniTest::Mock.new

      with_shopify_session do
        mock.expect(:call, nil, [billing,
                                 ShopifyAPI::RecurringApplicationCharge.new])
        Gold.configuration.on_check_charge = mock

        CheckChargeOp.new(billing).call
      end

      assert_mock mock
    ensure
      Gold.configuration.on_check_charge = nil
    end

    def test_charge_not_needed_when_free
      set_up_shop_as_billing!(:free)

      refute billing.can_transition_to?(:check_charge)
    end

    private

    def verify_has_active_charge(billing)
      tier = billing.tier
      stub_get_charges([
        {
          name: "Old Plan",
          price: "15.22",
          status: "cancelled"
        },
        {
          status: "active",
          name: tier.id,
          price: tier.monthly_price.to_s,
          trial_days: tier.trial_days,
          test: false
        },
        {
          name: "Legacy",
          price: "19.99",
          status: "pending"
        }
      ])

      with_shopify_session do
        op = CheckChargeOp.new(billing).call
        assert op.ok?
        assert_instance_of Outcomes::ActiveCharge, op
      end

      assert_equal :billing, billing.current_state
    end

    def stub_get_charges(charges)
      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charges: charges
        }.to_json)
    end
  end
end

class Gold::BillingController < ApplicationController
  include ShopifyApp::LoginProtection

  def activate_charge
    current_shop.shopify_session
    @charge = ShopifyAPI::RecurringApplicationCharge.find(params['charge_id'])

    if @charge.status == "accepted"
      @charge.activate
      current_shop.subscribe(@charge)
    else
      flash[:alert] = "Charge was not processed"
      close_session and return
    end

    redirect_to main_app.root_url
  end

  def setup_billing
    charge = current_shop.create_charge(activate_charge_url)

    if charge.valid?
      redirect_to charge.confirmation_url
    else
      errors = charge.errors.full_messages.join(",")
      flash[:error] = "The charge could not be processed because #{errors}"
      close_session and return
    end
  end
end

module Gold
  module Admin
    # Main controller for Gold admin to be used in app
    class AdminController < ApplicationController
      credentials = Gold.configuration.admin_credentials
      http_basic_authenticate_with name: credentials[:user],
                                   password: credentials[:password]
    end
  end
end

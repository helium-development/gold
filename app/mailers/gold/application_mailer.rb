module Gold
  # Defaults for all Gold-related mailers.
  class ApplicationMailer < ActionMailer::Base
    default from: Gold.configuration.contact_email
    layout "mailer"
  end
end

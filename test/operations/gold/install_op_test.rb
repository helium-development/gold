require "test_helper"
require "shopify_interactions"
require "minitest/mock"

module Gold
  class InstallOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def teardown
      Gold.configuration.on_install = nil
    end

    def test_install_paying_shop
      outcome = InstallOp.new(billing).call
      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
      assert_equal :install, billing.current_state
    end

    def test_execute_callback_for_new_install
      mock = MiniTest::Mock.new
      mock.expect(:call, nil, [billing])
      Gold.configuration.on_install = mock

      InstallOp.new(billing).call

      assert_mock mock
    end

    def test_execute_callback_for_reinstall
      billing.transition_to!(:marked_as_uninstalled)
      billing.transition_to!(:uninstalled)

      mock = MiniTest::Mock.new
      mock.expect(:call, nil, [billing])
      Gold.configuration.on_install = mock

      InstallOp.new(billing).call

      assert_mock mock
    end

    # When we're not install the app, don't fire the callback hook
    def test_no_execute_callback_for_no_install
      billing.transition_to!(:install)
      billing.transition_to!(:accepted_terms)

      Gold.configuration.on_install = proc { |_b|
        raise NoMethodError, "This should not be called"
      }

      InstallOp.new(billing).call
    ensure
      Gold.configuration.on_install = nil
    end

    def test_reinstall_billing_shop
      with_shopify_session do
        transition_to! :install
        transition_to! :accepted_terms
        transition_to! :select_tier, tier_id: :basic
        transition_to! :sudden_charge, charge_id: 1
        transition_to! :marked_as_uninstalled
        transition_to! :uninstalled
      end
      outcome = InstallOp.new(billing).call
      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
      assert_equal :reinstalled, billing.current_state
    end
  end
end

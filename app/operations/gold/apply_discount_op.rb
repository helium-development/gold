module Gold
  module Outcomes
    CannotApplyDiscount = Class.new(Failure)
    SameDiscount = Class.new(Success)
  end

  # Applies a discount to a shop and creates a new charge for them.
  class ApplyDiscountOp
    include Outcomes

    def initialize(billing, percentage, return_url, test = false)
      @billing = billing
      @percentage = percentage.to_i
      @return_url = return_url
      @test = test
    end

    def call
      # Validate the discount
      if @percentage < 0 || @percentage >= 100
        return CannotApplyDiscount.new(:invalid_percentage)
      end

      return SameDiscount.new if @percentage == @billing.discount_percentage

      @billing.update(discount_percentage: @percentage)

      # If we can transition to apply discount, let's do that. This means we're
      # in a billing state and a new charge will be created. Otherwise, it's
      # OK to just set the `discount_percentage` field for future charges
      if @billing.can_transition_to?(:apply_discount)
        @billing.transition_to!(:apply_discount, percentage: @percentage)
        ChargeOp.new(@billing, @return_url, @test).call
      else
        Success.new
      end
    end
  end
end

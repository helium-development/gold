require "test_helper"
require "shopify_interactions"
require "minitest/mock"

module Gold
  class SuspendOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    # Ensure that an account can be suspended properly.
    def test_suspension_from_billing
      set_up_shop_as_billing!
      suspend_and_verify!
    end

    def test_suspension_from_affiliate
      self.shopify_plan = "affiliate"
      with_shopify_session do
        transition_to! :install
        transition_to! :accepted_terms
        transition_to! :select_tier, tier_id: Tier.find(:basic).id
        transition_to! :affiliate
      end
      suspend_and_verify!
    end

    # Ensure that the operation can be retried if it fails for some reason.
    def test_retry_suspension
      set_up_shop_as_billing!

      # Assume that suspension has been attempted before and `billing` was left
      # in the marked state.
      transition_to! :marked_as_suspended

      outcome = verify_email_delivery do
        with_shopify_session do
          SuspendOp.new(billing).call
        end
      end
      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
      assert_equal :suspended, billing.current_state
    end

    private

    def suspend_and_verify!
      outcome = verify_email_delivery do
        with_shopify_session do
          SuspendOp.new(billing).call
        end
      end
      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
      assert_equal :suspended, billing.current_state
    end

    # Verify that an email is requested to be sent as part of suspending an
    # account.
    def verify_email_delivery(&block)
      # Mock out the email delivery part and verify that it was called later
      mock_delivery = MiniTest::Mock.new
      mock_delivery.expect(:deliver_now, true)
      result = BillingMailer.stub(:suspension, mock_delivery, &block)
      mock_delivery.verify
      result
    end
  end
end

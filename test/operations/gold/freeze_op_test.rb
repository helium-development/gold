require "test_helper"
require "shopify_interactions"
require "minitest/mock"

module Gold
  class FreezeOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    def test_freeze_from_billing
      set_up_shop_as_billing!
      billing.shopify_plan_override = "frozen"
      outcome = with_shopify_session { FreezeOp.new(billing).call }
      assert outcome.ok?
      assert_instance_of Outcomes::Success, outcome
      assert_equal :frozen, billing.current_state
    end

    def test_freeze_to_check_charge_returns_to_frozen
      set_up_shop_as_billing! :basic
      billing.shopify_plan_override = "frozen"
      billing.transition_to! :frozen

      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charges: [{
            id: "111111111",
            status: "frozen",
            name: billing.tier.id,
            price: billing.tier.monthly_price.to_s,
            trial_days: billing.tier.trial_days,
            test: false
          }]
        }.to_json)

      outcome = with_shopify_session { CheckChargeOp.new(billing).call }
      assert_instance_of Outcomes::FrozenCharge, outcome
      assert_equal :frozen, billing.current_state
    end
  end
end

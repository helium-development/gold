module Gold
  # Describes a Shopify plan that a shop is tied to. This may change over the
  # lifetime of the shop. While this is a relatively simple wrapper around the
  # plan name string, it provides some nice query methods.
  class ShopifyPlan
    def initialize(plan)
      @plan = plan
    end

    def to_s
      plan
    end

    def ==(other)
      plan == other.plan
    end
    alias eql? ==

    delegate :hash, to: :plan

    # Returns true if this is a development (non-live) shop.
    def affiliate?
      plans = %w[affiliate partner_test]
      plans.include?(plan)
    end

    # Returns true if this is a shop owned by Shopify staff. This specifically
    # excludes Shopify Business shops, as we believe those are paid stores that
    # Shopify employees use for their own businesses.
    def staff?
      plans = %w[plus_partner_sandbox staff]
      plans.include?(plan)
    end

    # Returns true if this shop has been frozen by Shopify for non-payment.
    def frozen?
      plan == "frozen"
    end

    # Returns true if this shop has cancelled their Shopify subscription.
    def cancelled?
      plan == "cancelled"
    end

    # Returns true if this shop is currently paused by the merchant. Their
    # online store is not accessible in this situation.
    def dormant?
      plan == "dormant"
    end

    # Returns whether this shop is on a paid plan and is in good standing
    # currently. A paying shop is able to accept charges.
    def paying?
      !(affiliate? || staff? || frozen? || cancelled?)
    end

    # Returns whether this shop should be allowed to create an account
    # currently.
    def good?
      !bad?
    end

    # The opposite of `#good?`
    def bad?
      frozen? || cancelled?
    end

    protected

    attr_reader :plan
  end
end

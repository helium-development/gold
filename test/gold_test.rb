require "test_helper"

module Gold
  class Test < ActiveSupport::TestCase
    def setup
      @gold_test_charge_original = Gold.configuration.test_charge
    end

    def teardown
      Gold.configuration.test_charge = @gold_test_charge_original
    end

    def test_test_charge_when_true
      Gold.configuration.test_charge = true
      assert Gold.configuration.test_charge?
    end

    def test_test_charge_when_false
      Gold.configuration.test_charge = false
      refute Gold.configuration.test_charge?
    end

    def test_test_charge_when_nil
      Gold.configuration.test_charge = nil
      refute Gold.configuration.test_charge?
    end
  end
end

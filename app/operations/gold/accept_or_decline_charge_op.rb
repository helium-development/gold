module Gold
  module Outcomes
    DeclinedCharge = Class.new(Failure)
    CannotProcessCharge = Class.new(Failure)
  end

  # The charge was accepted or denied by the merchant.
  class AcceptOrDeclineChargeOp
    include Outcomes
    include Retries

    def initialize(billing, charge_id)
      @billing = billing
      @charge_id = charge_id.to_i
      @charge_transition = @billing.state_machine.last_transition_to(
        :sudden_charge,
        :delayed_charge,
        :optional_charge
      )
    end

    def call
      unless @charge_transition
        Gold.logger.warn("[#{@billing.id}] Cannot find any charge creation events")
        return CannotProcessCharge.new(:not_found)
      end

      expected_charge_id = @charge_transition.metadata["charge_id"]

      if expected_charge_id != @charge_id
        Gold.logger.warn(
          "[#{@billing.id}] Expecting charge to be #{expected_charge_id.inspect} " \
          "but it was #{@charge_id.inspect}"
        )
        return CannotProcessCharge.new(:old_charge)
      end

      charge = find_charge

      if !charge
        CannotProcessCharge.new(:not_found)
      elsif charge.status == "active"
        Gold.logger.info("[#{@billing.id}] This charge has already been activated")
        @billing.transition_to!(accepted_state)
        @billing.transition_to!(activated_state)
        return ActiveCharge.new(@charge_id)
      elsif charge.status == "accepted"
        @billing.transition_to!(accepted_state)
        activate_charge(charge)
      elsif charge.status == "declined"
        case @charge_transition.to_state.to_sym
        when :sudden_charge
          @billing.transition_to!(:sudden_charge_declined)
        when :delayed_charge
          @billing.transition_to!(:delayed_charge_declined)
        when :optional_charge
          # Also see FindOutstandingCharge, as it does the same logic
          @billing.transition_to_or_stay_in!(:optional_charge_declined)
          @billing.transition_to!(:billing)
        end

        DeclinedCharge.new
      elsif charge.status == "expired"
        @billing.transition_to!(expired_state)
        ExpiredCharge.new
      else
        Gold.logger.warn("[#{@billing.id}] Charge failed '#{charge.status}'")
        CannotProcessCharge.new(:bad_state)
      end
    rescue ActiveResource::ForbiddenAccess
      @billing.transition_to!(:marked_as_uninstalled)
      Outcomes::Uninstalled.new
    end

    private

    def accepted_state
      case @charge_transition.to_state.to_sym
      when :sudden_charge then :sudden_charge_accepted
      when :delayed_charge then :delayed_charge_accepted
      when :optional_charge then :optional_charge_accepted
      else
        raise "Don't know what to do with #{@charge_transition.to_state}"
      end
    end

    def activated_state
      :charge_activated
    end

    def expired_state
      case @charge_transition.to_state.to_sym
      when :sudden_charge then :sudden_charge_expired
      when :delayed_charge then :delayed_charge_expired
      when :optional_charge then :optional_charge_declined
      else
        raise "Don't know what to do with #{@charge_transition.to_state}"
      end
    end

    def find_charge
      request_with_retries do
        ShopifyAPI::RecurringApplicationCharge.find(@charge_id)
      end
    rescue ActiveResource::ResourceNotFound
      Gold.logger.warn("[#{@billing.id}] failed to retrieve charge from Shopify")
      nil
    end

    # Consumes a charge and activates it
    def activate_charge(charge)
      request_with_retries do
        charge.activate
        @billing.transition_to!(activated_state)
        Gold.configuration.on_activate_charge&.call(@billing, charge)
        return ActiveCharge.new(charge.id)
      end
    end
  end
end

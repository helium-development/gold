module Gold
  module Outcomes
    MismatchCharge = Class.new(Failure)
  end

  # Ensure that the charge is correct and active.
  class CheckChargeOp
    include Outcomes
    include Retries

    def initialize(billing, test_charge = Gold.configuration.test_charge?)
      @billing = billing
      @test_charge = test_charge
    end

    def call
      @billing.transition_to(:check_charge) unless @billing.current_state == :check_charge

      charges = request_with_retries do
        ShopifyAPI::RecurringApplicationCharge.all || []
      end

      active_charge = charges.find { |charge| charge.status == "active" }
      frozen_charge = charges.find { |charge| charge.status == "frozen" }

      outcome = if active_charge && billing_matches_charge?(active_charge)
                  @billing.transition_to!(:billing)
                  ActiveCharge.new(active_charge.id)
                elsif frozen_charge && @billing.transition_to(:frozen)
                  FrozenCharge.new
                elsif active_charge
                  @billing.transition_to!(:charge_missing)
                  MismatchCharge.new
                else
                  @billing.transition_to!(:charge_missing)
                  MissingCharge.new
                end

      Gold.configuration.on_check_charge&.call(@billing, active_charge)
      outcome
    end

    private

    def billing_matches_charge?(charge)
      tier = @billing.last_selected_tier

      @billing.calculate_price(tier) == charge.price.to_d && # Price must match
        @billing.trial_starts_at && # Trial must have already started
        @test_charge == (charge.test || false)
    end
  end
end

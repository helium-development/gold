module Gold
  # Look up billing information and see if they have a charge or not based on
  # the charges we've previously created
  class ResolveOutstandingChargeOp
    include Outcomes
    include Retries

    def initialize(billing)
      @billing = billing
    end

    def call
      return MissingCharge.new(:no_charge_state) unless charge_state_transition

      charge_id = charge_state_transition.metadata["charge_id"]

      begin
        charge = request_with_retries do
          ShopifyAPI::RecurringApplicationCharge.find(charge_id)
        end
      rescue ActiveResource::ResourceNotFound
        return MissingCharge.new(:shopify_missing)
      end

      case charge.status
      when "active", "frozen"
        return ActiveCharge.new(charge_id)
      when "accepted"
        return AcceptedCharge.new(charge.decorated_return_url)
      when "pending"
        return PendingCharge.new(charge.confirmation_url)
      when "expired"
        if sudden_charge?
          @billing.transition_to!(:sudden_charge_expired)
          return ExpiredCharge.new
        elsif delayed_charge?
          @billing.transition_to!(:delayed_charge_expired)
          return ExpiredCharge.new
        elsif optional_charge?
          # It is OK for an optional charge to expire. Put the merchant back in
          # a normal billing state and let them retry their charge later, if
          # desired.
          @billing.transition_to!(:optional_charge_declined)
          @billing.transition_to!(:billing)
          return ActiveCharge.new(charge_id)
        end
        @billing.transition_to!(expired_state)
        return ExpiredCharge.new
      when "declined"
        if sudden_charge?
          @billing.transition_to!(:sudden_charge_declined)
          return MissingCharge.new(:charge_declined)
        elsif delayed_charge?
          @billing.transition_to!(:delayed_charge_declined)
          return MissingCharge.new(:charge_declined)
        elsif optional_charge?
          @billing.transition_to_or_stay_in!(:optional_charge_declined)
          @billing.transition_to!(:billing)
          return ActiveCharge.new(charge_id)
        end
      when "cancelled"
        return MissingCharge.new(:charge_cancelled)
      else
        raise MissingCharge.new(:bad_status, "Bad status: '#{charge.status}'")
      end
    end

    private

    def charge_state_transition
      @charge_state_transition ||=
        @billing.state_machine.last_transition_to(:sudden_charge,
                                                  :delayed_charge,
                                                  :optional_charge)
    end

    def sudden_charge?
      charge_state_transition.nil? ||
        charge_state_transition.to_state.to_sym == :sudden_charge
    end

    def delayed_charge?
      charge_state_transition.to_state.to_sym == :delayed_charge
    end

    def optional_charge?
      charge_state_transition.to_state.to_sym == :optional_charge
    end
  end
end

namespace :gold do
  desc "Generate a diagram of the Gold::Machine state machine"
  task diagram: :environment do
    # So that graphs export colors consistently
    random = Random.new(1)

    acceptance_states = lambda do |node|
      if Gold::Machine::ACCEPTANCE_STATES.include?(node.id.to_sym)
        node[:shape] = :doublecircle
      end
    end

    # Describes a transition between two states. Used as a Hash key.
    Transition = Struct.new(:from, :to)

    # Extract labels for guards.
    labels = {}
    Gold::Machine.callbacks[:guards].each do |guard|
      guard.to.each do |to|
        labels[Transition.new(guard.from, to)] = guard.callback.to_s
      end
    end

    labels_and_colors = lambda do |edge|
      color = %w[red darkgreen blue orange violet].sample(random: random)

      edge[:color] = color
      edge[:fontcolor] = color

      label = [
        labels[Transition.new(nil, edge.head_node)],
        labels[Transition.new(edge.tail_node, edge.head_node)]
      ].compact.join(" and ")
      if label
        edge[:label] = label
        edge[:fontsize] = 9
      end
    end

    diagram = Gold::Diagram.new(Gold::Machine,
                                node_callback: acceptance_states,
                                edge_callback: labels_and_colors)

    path = Rails.root.join("tmp", "gold_machine.svg")
    diagram.render(path)
    puts "Diagram generated and saved to #{path}"
  end

  desc "Checks charges for all billing instances"
  task check_charges: :environment do
    Gold::Billing.all.each do |billing|
      next unless billing.state_machine.can_transition_to?(:check_charge)

      billing.shop.with_shopify_session do
        outcome = Gold::CheckChargeOp.new(billing).call

        if outcome.ok?
          print "✅"
        else
          puts "Check charge for shop '#{billing.shop.shopify_domain}': #{outcome}"
          Gold::ChargeOp.new(billing,
                             Gold::Engine.routes.url_helpers.process_charge_url).call
        end
      rescue ActiveResource::UnauthorizedAccess
        puts "It looks like '#{billing.shop.shopify_domain}' uninstalled, running op..."
        Gold::UninstallOp.new(billing).call
      rescue ActiveResource::ClientError => e
        puts "Error for '#{billing.shop.shopify_domain}', #{e}"
      rescue Statesman::GuardFailedError => e
        puts "Cannot transition: #{e.message}"
      end
    end
  end

  desc "Move eligible billings into cleanup"
  task cleanup: :environment do
    Gold::Billing.all.each do |billing|
      next unless billing.state_machine.can_transition_to?(:cleanup)

      billing.shop.with_shopify_session do
        outcome = Gold::CleanupOp.new(billing).call
        puts "Cleanup for shop '#{billing.shop.shopify_domain}': #{outcome}"
      end
    end
  end

  desc "Move eligible billings into delinquence"
  task delinquents: :environment do
    Gold::Billing.all.each do |billing|
      unless billing.current_state == :marked_as_delinquent
        next unless billing.state_machine.can_transition_to?(:marked_as_delinquent)
      end

      billing.shop.with_shopify_session do
        domain = billing.shop.shopify_domain

        begin
          Gold::MarkAsDelinquentOp.new(billing).call
          puts "Shop '#{domain}' is delinquent"
        rescue ActiveResource::UnauthorizedAccess
          puts "It looks like '#{domain}' uninstalled, running op..."
          Gold::UninstallOp.new(billing).call
        rescue ActiveResource::ClientError => e
          puts "Error for '#{domain}', #{e}"
        end
      end
    end
  end
end

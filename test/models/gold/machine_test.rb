require "test_helper"
require "minitest/mock"
require "shopify_interactions"
require "gold/billing"

module Gold
  class MachineTest < ActiveSupport::TestCase
    def test_require_metadata
      Gold::Coverage.exclude_from_coverage do
        machine = Gold::Machine.new(nil)
        machine.transition_to!(:install)
        machine.transition_to!(:accepted_terms)
        assert_raises(Gold::Exceptions::MetadataMissing) do
          machine.transition_to!(:select_tier)
        end
        machine.transition_to!(:select_tier, tier_id: "special")
      end
    end
  end
end

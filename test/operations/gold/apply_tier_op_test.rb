require "test_helper"
require "shopify_interactions"
require "minitest/mock"

module Gold
  class ApplyTierOpTest < ActiveSupport::TestCase
    include ShopifyInteractions

    setup do
      @charge_id = "8657643421"
    end

    def set_up_shop_as_activated!(tier_id = :basic)
      with_shopify_session do
        transition_to!(:install)
        transition_to!(:accepted_terms)
        transition_to!(:select_tier, tier_id: tier_id)
        transition_to!(:sudden_charge, charge_id: @charge_id)
        transition_to!(:sudden_charge_accepted)
        transition_to!(:charge_activated)
      end
    end

    def set_up_shop_as_change_tier_to_free!(tier_id = :basic)
      set_up_shop_as_activated!(tier_id)
      transition_to!(:apply_tier, tier_id: tier_id)
      transition_to!(:billing)
      transition_to!(:change_tier, tier_id: :free)
    end

    def stub_get_charges(charges)
      stub_shopify_request(:get, "recurring_application_charges.json")
        .to_return(status: 200, body: {
          recurring_application_charges: charges
        }.to_json)
    end

    def test_applies_tier
      set_up_shop_as_activated!(:pro)
      billing.shopify_plan_override = "unlimited"

      op = ApplyTierOp.new(billing).call
      assert op.ok?
      assert_equal :billing, billing.current_state
      assert_equal :pro, billing.tier.id
      apply_tier = billing.state_machine.last_transition_to(:apply_tier)
      refute_nil apply_tier
      assert_equal "pro", apply_tier.metadata["tier_id"]
    end

    def test_invalid_tier_fails
      set_up_shop_as_activated!(:pengwaynes)

      op = ApplyTierOp.new(billing).call
      refute op.ok?
      assert_instance_of Outcomes::TierNotFound, op
      assert_nil billing.tier
    end

    def test_apply_free_tier_with_retries
      set_up_shop_as_change_tier_to_free!

      stub_get_charges([
        {
          id: @charge_id,
          name: Faker::App.name,
          status: "active",
          price: "10.99"
        }
      ])

      Gold.configuration.allow_automated_charge_cancellation = true

      path = "recurring_application_charges/#{@charge_id}.json"
      stub = stub_shopify_request(:delete, path).to_return(status: 500)
                                                .times(2).then
                                                .to_return(status: 200)

      with_shopify_session do
        op = ApplyTierOp.new(billing).call
        assert op.ok?
        assert_instance_of Outcomes::TierApplied, op
      end
      apply_free_tier = billing.state_machine
                               .last_transition_to(:apply_free_tier)
      refute_nil apply_free_tier
      assert_equal "free", apply_free_tier.metadata["tier_id"]

      assert_requested stub, times: 3
    end

    def test_apply_free_tier_with_no_charges
      set_up_shop_as_change_tier_to_free!
      stub_get_charges([])

      with_shopify_session do
        op = ApplyTierOp.new(billing).call
        assert op.ok?
      end
    end

    def test_can_retry_apply_tier
      set_up_shop_as_activated!
      transition_to!(:apply_tier, tier_id: billing.last_selected_tier.id)

      with_shopify_session do
        op = ApplyTierOp.new(billing).call
        assert op.ok?
      end
    end

    def test_can_retry_apply_free_tier
      transition_to!(:install)
      transition_to!(:accepted_terms)
      transition_to!(:select_tier, tier_id: :basic)
      transition_to!(:apply_free_tier, tier_id: :basic)

      with_shopify_session do
        op = ApplyTierOp.new(billing).call
        assert op.ok?
      end
    end

    def test_can_apply_free_tier_as_affiliate
      billing.shopify_plan_override = "affiliate"
      transition_to!(:install)
      transition_to!(:accepted_terms)
      transition_to!(:select_tier, tier_id: :basic)
      transition_to!(:apply_free_tier, tier_id: :basic)

      with_shopify_session do
        op = ApplyTierOp.new(billing).call
        assert op.ok?
      end
    end

    def test_can_apply_free_tier_as_staff
      billing.shopify_plan_override = "staff"
      transition_to!(:install)
      transition_to!(:accepted_terms)
      transition_to!(:select_tier, tier_id: :basic)
      transition_to!(:apply_free_tier, tier_id: :basic)

      with_shopify_session do
        op = ApplyTierOp.new(billing).call
        assert op.ok?
      end
    end

    def test_executes_callback_for_paid
      set_up_shop_as_activated!(:pro)
      billing.shopify_plan_override = "unlimited"
      mock = MiniTest::Mock.new
      mock.expect(:call, nil, [billing, :pro])
      Gold.configuration.on_apply_tier = mock

      ApplyTierOp.new(billing).call

      assert_mock mock
    ensure
      Gold.configuration.on_apply_tier = nil
    end
  end
end

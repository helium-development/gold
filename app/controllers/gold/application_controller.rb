module Gold
  # Controller that all Gold controllers inherit from.
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    around_action :catch_halt

    layout "application"

    helper_method :billing

    private

    # Allows an action to stop processing (like `return`, but works even in
    # nested calls) and return a response to the user.
    def catch_halt
      catch(:halt) { yield }
    end
  end
end

module Gold
  module Outcomes
    # This is the base class for any outcome. Sub-classes are intended to
    # inherit from this and define whether that outcome is considered ok or not.
    #
    # If any subclass defines an `initialize` method, it should call `super` to
    # ensure the `@ok` variable is set.
    class Outcome < StandardError
      def initialize(is_ok, message = nil)
        @ok = is_ok
        super(message)
      end

      def ok?
        @ok
      end
    end

    # General failure class
    class Failure < Outcome
      attr_reader :reason

      def initialize(reason = nil, message = nil)
        @reason = reason
        super(false, message || reason)
      end
    end

    # A general success class
    class Success < Outcome
      def initialize(message = nil)
        super(true, message)
      end
    end

    # Common outcomes
    Uninstalled = Class.new(Failure)

    # Outcomes that relate to tiers
    TierApplied = Class.new(Success)
    TierNotFound = Class.new(Failure)

    # Outcomes that relate to charges
    ChargeNeeded = Class.new(Success)
    ChargeNotNeeded = Class.new(Success)
    FrozenCharge = Class.new(Success)
    ExpiredCharge = Class.new(Failure)
    MissingCharge = Class.new(Failure)

    ActiveCharge = Class.new(Success) do
      attr_reader :charge_id

      def initialize(charge_id)
        @charge_id = charge_id
        super()
      end
    end

    AcceptedCharge = Class.new(Success) do
      attr_reader :return_url

      def initialize(return_url)
        @return_url = return_url
        super()
      end
    end

    PendingCharge = Class.new(Success) do
      attr_reader :confirmation_url

      def initialize(confirmation_url)
        @confirmation_url = confirmation_url
        super()
      end
    end
  end
end

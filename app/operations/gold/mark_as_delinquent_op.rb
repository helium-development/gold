module Gold
  # Invoked when Helium suspends a merchant for violating ToS.
  class MarkAsDelinquentOp
    include Outcomes

    def initialize(billing)
      @billing = billing
    end

    def call
      @billing.transition_to_or_stay_in!(:marked_as_delinquent)

      # Communicate delinquency to the merchant.
      BillingMailer.delinquent(@billing).deliver_now

      @billing.transition_to!(:delinquent)

      Success.new
    end
  end
end

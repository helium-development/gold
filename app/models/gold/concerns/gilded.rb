require "statesman"

module Gold
  module Concerns
    # This adds all of the necessary bits to an application's shop class for
    # Gold to work properly.
    #
    # Steps to install:
    #
    # 1) Include `Gold::Concerns::Gilded` in the Shop class,
    # 2) Run `rake gold:install:migrations`
    # 3) Run `rake db:migrate`
    #
    module Gilded
      extend ActiveSupport::Concern

      included do
        has_one :billing, class_name: "Gold::Billing", dependent: :destroy
      end
    end
  end
end
